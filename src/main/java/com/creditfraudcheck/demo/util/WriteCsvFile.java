package com.creditfraudcheck.demo.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.creditfraudcheck.demo.model.BulkCsvModel;
import com.creditfraudcheck.demo.model.QuickCreditAndFraudResponseResult;
import com.opencsv.CSVWriter;

@Component
public class WriteCsvFile {

	Logger logger = Logger.getLogger(WriteCsvFile.class.getName());

	public void writeCsvFile(QuickCreditAndFraudResponseResult quickCreditAndFraudResponseResult, String csvFileName,
			BulkCsvModel bulkCsvModel) throws IOException {
		Properties prop = null;
		logger.info("In writeCsvFile() ");

		File file = null;
		FileWriter fileWriter = null;
		CSVWriter csvWriter = null;
		BufferedReader bufferedReader = null;
		String csvData[] = addElementInCsvData(quickCreditAndFraudResponseResult, bulkCsvModel);
		int count = 0;

		try {

			Util util = new Util();
			prop = util.readPropertiesFile("application.properties");
			String csvFilePath = prop.getProperty("writeCsvFilePath");

			file = new File(csvFilePath + csvFileName);
			logger.info("write csv File path " + csvFilePath + csvFileName);
			if (file.exists()) {
				logger.info("CSV file already genrated so updating old csv file");
				fileWriter = new FileWriter(file, true);
				csvWriter = new CSVWriter(fileWriter);

				csvWriter.writeNext(csvData);
				logger.info("write csv file data " + Arrays.toString(csvData));
				logger.info("CSV file updated succesfully.");

				bufferedReader = new BufferedReader(new FileReader(file));
				String input;

				try {
					while ((input = bufferedReader.readLine()) != null) {
						count++;
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				logger.info("******The row has been processed upto now is " + count+"*******");

			} else {

				logger.info("CSV file already not genrated so creating new  csv file");

				fileWriter = new FileWriter(file, true);
				csvWriter = new CSVWriter(fileWriter);
				String[] header = { "INPUT_surname", "INPUT_forname1", "INPUT_id_no", "INPUT_cell", "Forename1",
						"Forename2", "Surname", "Telephonenumber", "Title", "Gender", "IdentityNumber1",
						"Marital status description", "Deceased date", "Credit score", "Line1", "Line2", "Suburb",
						"City", "Postal code", "Address Date", "Credit score reasons", "Rating", "Rating Description",
						"Reason Code", "Reason Description" };
				csvWriter.writeNext(header);

				csvWriter.writeNext(csvData);
				logger.info("write csv file data " + Arrays.toString(csvData));
				logger.info("CSV file updated succesfully.");

			}
			csvWriter.close();
			fileWriter.close();
			logger.info("writeCsvFile( ) method end");


		} catch (FileNotFoundException fe) {
			logger.info("FileNotFoundException" + fe);
			fe.printStackTrace();
		} catch (Exception e) {
			// TODO: handle exception
			logger.info("Exception" + e);
			e.printStackTrace();

		}
	}

	public String[] addElementInCsvData(QuickCreditAndFraudResponseResult quickCreditAndFraudResponseResult,
			BulkCsvModel bulkCsvModel) {

		String csvData[] = new String[25];

		csvData[0] = bulkCsvModel.getSurName();
		csvData[1] = bulkCsvModel.getForeName1();
		csvData[2] = bulkCsvModel.getIdNumber();
		csvData[3] = bulkCsvModel.getCellNumber();

		if (quickCreditAndFraudResponseResult.getForename1() != null) {
			csvData[4] = quickCreditAndFraudResponseResult.getForename1();
		} else {
			csvData[4] = "data not found";
		}

		if (quickCreditAndFraudResponseResult.getForename2() != null) {
			csvData[5] = quickCreditAndFraudResponseResult.getForename2();
		} else {
			csvData[5] = "data not found";
		}

		if (quickCreditAndFraudResponseResult.getSurname() != null) {
			csvData[6] = quickCreditAndFraudResponseResult.getSurname();
		} else {
			csvData[6] = "data not found";
		}

		if (quickCreditAndFraudResponseResult.getTelephoneNumbers() != null) {
			csvData[7] = quickCreditAndFraudResponseResult.getTelephoneNumbers();
		} else {
			csvData[7] = "data not found";
		}

		if (quickCreditAndFraudResponseResult.getGender() != null) {
			csvData[8] = quickCreditAndFraudResponseResult.getGender();
		} else {
			csvData[8] = "data not found";
		}

		if (quickCreditAndFraudResponseResult.getTitle() != null) {
			csvData[9] = quickCreditAndFraudResponseResult.getTitle();
		} else {
			csvData[9] = "data not found";
		}

		if (quickCreditAndFraudResponseResult.getIdentityNo1() != null) {
			csvData[10] = quickCreditAndFraudResponseResult.getIdentityNo1();
		} else {
			csvData[10] = "data not found";
		}

		if (quickCreditAndFraudResponseResult.getMaritalStatusDesc() != null) {
			csvData[11] = quickCreditAndFraudResponseResult.getMaritalStatusDesc();
		} else {
			csvData[11] = "data not found";
		}

		if (quickCreditAndFraudResponseResult.getDeceasedDate() != null) {
			csvData[12] = quickCreditAndFraudResponseResult.getDeceasedDate();
		} else {
			csvData[12] = "data not found";
		}

		if (quickCreditAndFraudResponseResult.getCredit_Score() != null) {
			csvData[13] = quickCreditAndFraudResponseResult.getCredit_Score();
		} else {
			csvData[13] = "data not found";
		}

		if (quickCreditAndFraudResponseResult.getLine1() != null) {
			csvData[14] = quickCreditAndFraudResponseResult.getLine1();
		} else {
			csvData[14] = "data not found";
		}

		if (quickCreditAndFraudResponseResult.getLine2() != null) {

			csvData[15] = quickCreditAndFraudResponseResult.getLine2();
		} else {
			csvData[15] = "data not found";
		}

		if (quickCreditAndFraudResponseResult.getSuburb() != null) {

			csvData[16] = quickCreditAndFraudResponseResult.getSuburb();
		} else {
			csvData[16] = "data not found";
		}

		if (quickCreditAndFraudResponseResult.getCity() != null) {

			csvData[17] = quickCreditAndFraudResponseResult.getCity();
		} else {
			csvData[17] = "data not found";
		}

		if (quickCreditAndFraudResponseResult.getPostalCode() != null) {

			csvData[18] = quickCreditAndFraudResponseResult.getPostalCode();
		} else {
			csvData[18] = "data not found";
		}

		if (quickCreditAndFraudResponseResult.getInformationDate() != null) {

			csvData[19] = quickCreditAndFraudResponseResult.getInformationDate();
		} else {
			csvData[19] = "data not found";
		}

		if (quickCreditAndFraudResponseResult.getScoreReasons() != null) {

			csvData[20] = quickCreditAndFraudResponseResult.getScoreReasons();
		} else {
			csvData[20] = "data not found";
		}

		if (quickCreditAndFraudResponseResult.getRating() != null) {

			csvData[21] = quickCreditAndFraudResponseResult.getRating();
		} else {
			csvData[21] = "data not found";
		}

		if (quickCreditAndFraudResponseResult.getRatingDescription() != null) {

			csvData[22] = quickCreditAndFraudResponseResult.getRatingDescription();
		} else {
			csvData[22] = "data not found";
		}

		if (quickCreditAndFraudResponseResult.getFraudReasonCode() != null) {

			csvData[23] = quickCreditAndFraudResponseResult.getFraudReasonCode();
		} else {
			csvData[23] = "data not found";
		}

		if (quickCreditAndFraudResponseResult.getFraudReasonDescription() != null) {

			csvData[24] = quickCreditAndFraudResponseResult.getFraudReasonDescription();
		} else {
			csvData[24] = "data not found";
		}

		return csvData;

	}

}
