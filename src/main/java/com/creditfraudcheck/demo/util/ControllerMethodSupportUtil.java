package com.creditfraudcheck.demo.util;

import java.io.IOException;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.creditfraudcheck.demo.dao.UserHistoryDao;
import com.creditfraudcheck.demo.model.BulkCsvModel;
import com.creditfraudcheck.demo.model.QuickCreditAndFraudResponseResult;

@Component
public class ControllerMethodSupportUtil {

	Logger logger = Logger.getLogger(ControllerMethodSupportUtil.class.getName());
	@Autowired
	TraceUserSmartleadsv2ApiRequest traceUserSmartleadsv2ApiRequest;
	@Autowired
	DoQuickCreditAndFraudApiRequest doQuickCreditAndFraudApiRequest;
	@Autowired
	WriteCsvFile writeCsvFile;
	@Autowired
	UserHistoryDao userHistoryDao;

	// calling trace user OKHTTP request method and then calling credit fraud check
	// API request method
	public void traceUserApiCalledSupportingMethod(BulkCsvModel bulkCsvModel, boolean isBulkCsvUploadFlag,
			String csvFileName, String emailAddress) throws SQLException {

		logger.info("In traceUserApiCalledSupportingMethod() for  cell number  " + bulkCsvModel.getCellNumber()
				+ " and ,last name " + bulkCsvModel.getSurName() + " and , csv file name " + csvFileName);
		String[] responseXmlAndResponseMessage = null;
		String[] traceUserApiParseValues;
		String traceUserSmartleadsv2ApiJsonResult = "";
		String lastName = "";
		String firstName = "";
		String idNumber = "";
		QuickCreditAndFraudResponseResult quickCreditAndFraudResponseResult = new QuickCreditAndFraudResponseResult();
		String cellNumber = bulkCsvModel.getCellNumber();

		try {

			if (cellNumber.startsWith("0")) {
				cellNumber = bulkCsvModel.getCellNumber();
			} else {
				cellNumber = "0" + bulkCsvModel.getCellNumber();
			}

			traceUserSmartleadsv2ApiJsonResult = traceUserSmartleadsv2ApiRequest
					.traceUserSmartleadsv2ApiRequest(bulkCsvModel.getSurName(), cellNumber);

		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// if trace user API result getting not null
		if (traceUserSmartleadsv2ApiJsonResult.trim().length() > 0 && traceUserSmartleadsv2ApiJsonResult != null) {

			logger.info("traceUserSmartleadsv2ApiJsonResult is not null so parsing the response");

			traceUserApiParseValues = traceUserSmartleadsv2ApiRequest
					.parsTraceUserSmartleadsv2ApiResponse(traceUserSmartleadsv2ApiJsonResult);
			lastName = traceUserApiParseValues[0];
			firstName = traceUserApiParseValues[1];
			idNumber = traceUserApiParseValues[2];

			try {

				responseXmlAndResponseMessage = doQuickCreditAndFraudApiRequest.doQuickCreditAndFraudRequest(
						firstName.trim(), lastName.trim(), idNumber.trim(), isBulkCsvUploadFlag);

			} catch (ClassNotFoundException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.error(e.getMessage());
			}
			String doQuickCreditAndFraudResponseXMLResult = responseXmlAndResponseMessage[0];

			logger.info(" doQuickCreditAndFraudResponseXMLResult " + doQuickCreditAndFraudResponseXMLResult);

			String responseMessage = responseXmlAndResponseMessage[1];

			logger.info(" responseMessage " + responseMessage);

			// response message getting from quick credit fraud API is not equal to Internal
			// Server Error
			if (responseMessage != null && responseMessage.trim().length() > 0) {

				logger.info("responseMessage is not null so parsing the response");

				if (!responseMessage.equals("Internal Server Error")) {

					if (doQuickCreditAndFraudResponseXMLResult != null
							&& doQuickCreditAndFraudResponseXMLResult.trim().length() > 0) {

						logger.info("doQuickCreditAndFraudResponseXMLResult is not null so parsing the response");

						try {

							quickCreditAndFraudResponseResult = doQuickCreditAndFraudApiRequest
									.getQuickCreditAndFraudResponseResult(doQuickCreditAndFraudResponseXMLResult,
											firstName.trim(), lastName.trim(), idNumber.trim(), cellNumber,
											emailAddress, responseMessage, isBulkCsvUploadFlag);
							try {

								logger.info("calling write csv file method ");

								writeCsvFile.writeCsvFile(quickCreditAndFraudResponseResult, csvFileName, bulkCsvModel);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								logger.error(e.getMessage());
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							logger.error(e.getMessage());
						}
					} else {

						logger.info("doQuickCreditAndFraudResponseXMLResult is  null so directly writing csv file");

						try {
							writeCsvFile.writeCsvFile(quickCreditAndFraudResponseResult, csvFileName, bulkCsvModel);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							logger.error(e.getMessage());
						}
					}
				}

				else {

					logger.info(
							"doQuickCreditAndFraudResponseXMLResult is  equal to Internal Server Error  so directly writing csv file");

					try {
						writeCsvFile.writeCsvFile(quickCreditAndFraudResponseResult, csvFileName, bulkCsvModel);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						logger.error(e.getMessage());
					}
				}

			}
			// response message getting from quick credit fraud API is equal to Internal
			// Server Error
			else {
				logger.info("responseMessage is  null so directly writing csv file");

				try {
					writeCsvFile.writeCsvFile(quickCreditAndFraudResponseResult, csvFileName, bulkCsvModel);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					logger.error(e.getMessage());
				}
			}

		} else {
			// else trace user API result getting null

			logger.info("traceUserSmartleadsv2ApiJsonResult is  null so directly writing csv file");

			try {
				// saving data to user history table
				userHistoryDao.insertToUserHistoryTable("doQuickCreditAndFraud_BULK",
						emailAddress, bulkCsvModel.getForeName1(),
						bulkCsvModel.getSurName(), bulkCsvModel.getIdNumber(),
						bulkCsvModel.getCellNumber(), "data not found", "", "");
				
				writeCsvFile.writeCsvFile(quickCreditAndFraudResponseResult, csvFileName, bulkCsvModel);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}

	}
	// calling credit fraud check API request method

	public void creditFraudCheckApiSupportMethod(BulkCsvModel bulkCsvModel, boolean isBulkCsvUploadFlag,
			String csvFileName, String emailAddress) {

		String[] responseXmlAndResponseMessage = null;
		QuickCreditAndFraudResponseResult quickCreditAndFraudResponseResult = new QuickCreditAndFraudResponseResult();

		logger.info("In creditFraudCheckApiSupportMethod() for  id number  " + bulkCsvModel.getIdNumber()
				+ " and ,last name " + bulkCsvModel.getSurName() + " and , first name " + bulkCsvModel.getForeName1()
				+ " and , csv file name " + csvFileName);

		try {
			responseXmlAndResponseMessage = doQuickCreditAndFraudApiRequest.doQuickCreditAndFraudRequest(
					bulkCsvModel.getForeName1().trim(), bulkCsvModel.getSurName().trim(),
					bulkCsvModel.getIdNumber().trim(), isBulkCsvUploadFlag);

		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		String doQuickCreditAndFraudResponseXMLResult = responseXmlAndResponseMessage[0];

		String responseMessage = responseXmlAndResponseMessage[1];

		logger.info(" doQuickCreditAndFraudResponseXMLResult " + doQuickCreditAndFraudResponseXMLResult);

		logger.info(" responseMessage " + responseMessage);

		if (responseMessage != null && responseMessage.trim().length() > 0) {
		//  if condition to check quick credit fraud check API  response  message is not null

			logger.info("responseMessage is not null so parsing the response");

			if (!responseMessage.equals("Internal Server Error")) {
				
			//  if condition to check quick credit fraud check API  response  message is not equal to Internal Server Error

				logger.info("responseMessage is equal to Internal Server Error ");

				if (doQuickCreditAndFraudResponseXMLResult != null
						&& doQuickCreditAndFraudResponseXMLResult.trim().length() > 0) {

					logger.info("doQuickCreditAndFraudResponseXMLResult is not null so parsing the response");

					try {
						//  if condition to check quick credit fraud check API  response result is not null


						quickCreditAndFraudResponseResult = doQuickCreditAndFraudApiRequest
								.getQuickCreditAndFraudResponseResult(doQuickCreditAndFraudResponseXMLResult,
										bulkCsvModel.getForeName1().trim(), bulkCsvModel.getSurName().trim(),
										bulkCsvModel.getIdNumber().trim(), "", // cell number
										emailAddress, responseMessage, isBulkCsvUploadFlag); // emailAddress
						try {

							logger.info("calling write csv file method ");

							writeCsvFile.writeCsvFile(quickCreditAndFraudResponseResult, csvFileName, bulkCsvModel);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							logger.error(e.getMessage());
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						logger.error(e.getMessage());
					}

				} else {
					logger.info("doQuickCreditAndFraudResponseXMLResult is  null so directly writing csv file");

					try {
						writeCsvFile.writeCsvFile(quickCreditAndFraudResponseResult, csvFileName, bulkCsvModel);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						logger.error(e.getMessage());
					}
				}

			} else {

				logger.info(
						"doQuickCreditAndFraudResponseXMLResult is  equal to Internal Server Error  so directly writing csv file");

				try {

					writeCsvFile.writeCsvFile(quickCreditAndFraudResponseResult, csvFileName, bulkCsvModel);

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					logger.error(e.getMessage());
				}
			}

		} else {
			logger.info("responseMessage is  null so directly writing csv file");

			try {

				writeCsvFile.writeCsvFile(quickCreditAndFraudResponseResult, csvFileName, bulkCsvModel);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}

	}

}
