package com.creditfraudcheck.demo.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MailUtility {

	static Logger logger = Logger.getLogger(MailUtility.class.getName());
	@Autowired
	Util util;

	public String sendEmailAddressAndPwdToAdmin(String emailAddress, String password) {

		Properties prop = null;
		String mailTemplate="";
		try {
			logger.info("In sendEmailAddressAndPwdToAdmin method for  email address: " + emailAddress);

			prop = util.readPropertiesFile("application.properties");
			File htmlFile = new File(prop.getProperty("userDetailsMailTemplatePath"));
			mailTemplate = new String(Files.readAllBytes(htmlFile.toPath()));
			
			if (!htmlFile.exists())
				throw new FileNotFoundException();
			final String Form = prop.getProperty("mail.from");
			String to = System.getProperty("mailTo");

			//String to = prop.getProperty("mail.to");
			final String pwd = prop.getProperty("mail.pwd");

			// mailTempalte data
			mailTemplate = mailTemplate.replace("[emailAddress]", emailAddress);
			mailTemplate = mailTemplate.replace("[Password]", password);

			String host = "smtp.gmail.com";
			prop.put("mail.smtp.auth", "true");
			prop.put("mail.smtp.starttls.enable", "true");
			prop.put("mail.smtp.host", host);
			prop.put("mail.smtp.port", "587");

			// Get the Session object
			Session session = Session.getInstance(prop, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(Form, pwd);
				}
			});
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(Form));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject("Login details of new registered user ");
			message.setContent(mailTemplate, "text/html; charset=utf-8");
			Transport.send(message);
			logger.info("email send sucessfully !---");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		} catch (FileNotFoundException ex) {
			logger.error(" File does not exist in the path", ex);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.print(e);
			logger.error(e.getMessage(), e);

		}
		return mailTemplate;
	}

}
