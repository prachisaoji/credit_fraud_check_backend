package com.creditfraudcheck.demo.util;

import java.io.EOFException;
import java.io.IOException;
import java.io.StringReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.creditfraudcheck.demo.dao.QuickCreditAndFraudResponseDao;
import com.creditfraudcheck.demo.dao.UserHistoryDao;
import com.creditfraudcheck.demo.model.QuickCreditAndFraudResponseResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@Component
public class DoQuickCreditAndFraudApiRequest {
	@Autowired
	UserHistoryDao userHistoryDao;
	@Autowired
	Util util;
	@Autowired
	QuickCreditAndFraudResponseDao tempUriDao;
	@Autowired
	DoQuickCreditFraudApiUtility doQuickCreditFraudApiUtility;

	Logger logger = Logger.getLogger(DoQuickCreditAndFraudApiRequest.class.getName());

	public String[] doQuickCreditAndFraudRequest(String firstName, String lastName, String idNumber,
			boolean isBulkCsvUploadFlag) throws EOFException, ClassNotFoundException, IOException {
		boolean doCredit = true;
		boolean doFraud = true;
		String xmlInput = "";
		String responseXmlAndResponseMessage[] = new String[4];

		Properties prop = util.readPropertiesFile("tUQuickCreditAndFraudApi.properties");
		Gson gsonUtiltiy = new Gson();
		String apiUrl = prop.getProperty("apiUrl");
		OkHttpClient client = null;

		logger.info(" In  doQuickCreditAndFraudRequest() method  for first name " + firstName + " , last name "
				+ lastName + "  and idNumber " + idNumber);
		String soap_string = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
				+ "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
				+ "<soap:Body>" + "<doQuickCreditAndFraud xmlns=\"http://tempuri.org/\">" + "<doFraud>" + doFraud
				+ "</doFraud>" + "<doCredit>" + doCredit + "</doCredit>" + "<username>" + prop.getProperty("username")
				+ "</username>" + "<password>" + prop.getProperty("password") + "</password>" + "<contactFirstName>"
				+ firstName + "</contactFirstName>" + "<contactLastName>" + lastName + "</contactLastName>"
				+ "<contactID>" + idNumber + "</contactID>" + "<clientNumber>" + prop.getProperty("clientNumber")
				+ "</clientNumber>" + "<clientEmail>" + prop.getProperty("clientEmail") + "</clientEmail>" + "<apiKey>"
				+ prop.getProperty("apiKey") + "</apiKey>" + "</doQuickCreditAndFraud>" + "</soap:Body>"
				+ "</soap:Envelope>";

		logger.info("soap_string " + soap_string);

		if (isBulkCsvUploadFlag == true) {

			logger.info(" In  doQuickCreditAndFraudRequest() method  is it bulk csv upload");
			client = new OkHttpClient.Builder().connectTimeout(130, TimeUnit.SECONDS) // connect timeout
					.writeTimeout(130, TimeUnit.SECONDS) // write timeout
					.readTimeout(130, TimeUnit.SECONDS) // read timeout
					.build();
		}

		else {

			logger.info(" In  doQuickCreditAndFraudRequest() method  itis not bulk csv upload");

			client = new OkHttpClient.Builder().connectTimeout(30, TimeUnit.SECONDS) // connect timeout
					.writeTimeout(30, TimeUnit.SECONDS) // write timeout
					.readTimeout(30, TimeUnit.SECONDS) // read timeout
					.build();
		}

		// OkHttpClient client = new OkHttpClient();

		// client.setConnectTimeout(2, TimeUnit.MINUTES); // connect timeout
		// client.setReadTimeout(2, TimeUnit.MINUTES); // socket timeout

		MediaType mediaType = MediaType.parse("text/xml");

		RequestBody body = RequestBody.create(mediaType, soap_string);
		Request requestForPartSearch = new Request.Builder().url(apiUrl).post(body)
				.addHeader("content-type", "text/xml").build();

		Response response = null;

		try {
			response = client.newCall(requestForPartSearch).execute();
			System.out.println(gsonUtiltiy.toJson(" Response : " + response));
			logger.info("TUQuickCreditAndFraud response get successfully");
			String responseMessage = response.message();

			xmlInput = response.body().string(); // <-- The XML SOAP response
			responseXmlAndResponseMessage[0] = xmlInput;
			responseXmlAndResponseMessage[1] = responseMessage;
			logger.info(" response message we are getting from api response "+responseMessage);

			logger.info(" In  doQuickCreditAndFraudRequest() method end");
		}

		catch (EOFException eof) {
			eof.printStackTrace();
			logger.info("EOFException" + eof);
			System.out.println(eof);
		} catch (IOException ie) {
			ie.printStackTrace();
			logger.info("IOException" + ie);
			System.out.println(ie);
		}

		catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception" + e);

			System.out.println(e);
		}

		return responseXmlAndResponseMessage;
	}

	public Map<String, Object> parseDoQuickCreditAndFraudResultXmmldata(String xmlInput, String searchFirstName,
			String searchLastName, String searchIdNumber, String emailAddress, String responseMessage)
			throws JsonMappingException, JsonProcessingException, JSONException, SQLException {

		DocumentBuilder builder = null;
		String doQuickCreditAndFraudResult = "";
		Map<String, Object> convertJsonToMapObject = new HashMap<String, Object>();
		String dateRequestedString = "";
		String scoreReasons = "";
		String scoreValue = "";
		QuickCreditAndFraudResponseDao tempUriDao = new QuickCreditAndFraudResponseDao();

		logger.info("In parseDoQuickCreditAndFraudResultXmmldata() method  for first name " + searchFirstName
				+ " , last name " + searchLastName + " " + " and, idNumber " + searchIdNumber + ", and  emailAddress"
				+ emailAddress);
		QuickCreditAndFraudResponseResult quickCreditAndFraudResponseResult = new QuickCreditAndFraudResponseResult();
		try {
			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException pce) {
			// TODO Auto-generated catch block
			pce.printStackTrace();
			logger.info("ParserConfigurationException" + pce);
		}
		InputSource src = new InputSource();
		src.setCharacterStream(new StringReader(xmlInput));

		Document doc = null;
		try {
			doc = builder.parse(src);
		} catch (SAXException saxe) {
			// TODO Auto-generated catch block
			logger.info("SAXException" + saxe);
			saxe.printStackTrace();
		} catch (IOException ioe) {
			// TODO Auto-generated catch block
			logger.info("IOException" + ioe);
			ioe.printStackTrace();
		}

		doQuickCreditAndFraudResult = doc.getElementsByTagName("doQuickCreditAndFraudResult").item(0).getTextContent();
		logger.info(" doQuickCreditAndFraudResult " + doQuickCreditAndFraudResult);
		logger.info("In parseDoQuickCreditAndFraudResultXmmldata() get json doQuickCreditAndFraudResult from the xml ");

		try {
			JSONArray rootJsonArray = new JSONArray(doQuickCreditAndFraudResult);
			JSONObject creaditResultJsonObject = new JSONObject();
			creaditResultJsonObject = rootJsonArray.getJSONObject(0);

			Object rootJson = creaditResultJsonObject.get("Value");

			JSONArray parentJsonArray = new JSONArray(rootJson.toString());
			ObjectMapper objectMapper = new ObjectMapper();

			JsonNode consumerInfoNode = objectMapper.readValue(parentJsonArray.getJSONArray(0).get(0).toString(),
					JsonNode.class);
			JsonNode consumerInfoValue = consumerInfoNode.get("Value");

			if (!consumerInfoValue.isNull() && consumerInfoValue != null && !consumerInfoValue.isEmpty()) {
				convertJsonToMapObject = jsonToMap(creaditResultJsonObject);
				logger.info("converting json to map ");

				JsonNode creditScoreNode = objectMapper.readValue(parentJsonArray.getJSONArray(1).get(0).toString(),
						JsonNode.class);
				JSONObject creditScoreJsonObject = new JSONObject(creditScoreNode.toString());
				JSONObject creditScoreJsonResponse = (JSONObject) creditScoreJsonObject.get("Value");

				scoreReasons = creditScoreJsonResponse.get("ScoreReasons").toString();
				scoreReasons = scoreReasons.replaceAll("\\[(.*?)\\]", "$1");
				scoreReasons = scoreReasons.replace("\"", "");
				System.out.println("Getting scoreReasons from CreditScore json are : " + scoreReasons.toString());

				String Indicators = creditScoreJsonResponse.get("Indicators").toString();
				System.out.println("getting Indicators from CreditScore json are :" + Indicators.toString());

				JSONArray indicatorsJsonArray = (JSONArray) creditScoreJsonResponse.get("Indicators");
				JSONObject indicatorsJsonObject = indicatorsJsonArray.getJSONObject(0);
				scoreValue = indicatorsJsonObject.getString("Score");
				System.out.println("getting scoreValue Indicators json arry are :" + scoreValue.toString());

			} else// if no search data available in transunion
			{
				responseMessage = "data not found";
				scoreReasons = "not found";
				scoreValue = "not found";
			}

			logger.info("getting Date Requested from json");

			JsonNode dateRequestedNode = objectMapper.readValue(parentJsonArray.getJSONArray(3).get(0).toString(),
					JsonNode.class);
			JSONObject dateRequestedJsonObject = new JSONObject(dateRequestedNode.toString());
			dateRequestedString = dateRequestedJsonObject.get("Value").toString();

			// adding details to user_history table
			// userHistoryDao.insertToUserHistoryTable("doQuickCreditAndFraud",
			// emailAddress.trim(),
			// searchFirstName.trim(), searchLastName.trim(), searchIdNumber.trim(),
			// responseMessage,
			// scoreValue.trim(), scoreReasons.trim());

			if (dateRequestedString != null) {
				dateRequestedString = dateRequestedJsonObject.get("Value").toString();
			}

			logger.info(
					"saved doQuickCreditAndFraudResult json result,searchFirstName,searchLastName,searchIdNumber,dateRequestedString to database ");
			// inserting doQuickCreditAndFraudResult json
			// result,searchFirstName,searchLastName,searchIdNumber,dateRequestedString to
			// database
			tempUriDao.insertQuickCreditAndFraudResponseToDb(doQuickCreditAndFraudResult, searchFirstName,
					searchLastName, searchIdNumber, dateRequestedString);// dateRequestedString=searchDateAndTime

			logger.info(" getDoQuickCreditAndFraudResultXmmldata() method end ");

		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception" + e);
			System.out.println(e);
		}
		return convertJsonToMapObject;
	}

	public QuickCreditAndFraudResponseResult getQuickCreditAndFraudResponseResult(String xmlInput,
			String searchFirstName, String searchLastName, String searchIdNumber, String searchCellNumber,
			String emailAddress, String responseMessage, boolean isBulkCsvUploadFlag) {
		DocumentBuilder builder = null;
		String doQuickCreditAndFraudResult = "";
		String dateRequestedString = "";
		String scoreReasons = "";
		String scoreValue = "";
		QuickCreditAndFraudResponseResult quickCreditAndFraudResponseResult = new QuickCreditAndFraudResponseResult();

		logger.info("In getQuickCreditAndFraudResponseResult() method  for first name " + searchFirstName
				+ " , last name " + searchLastName + " " + " and, idNumber " + searchIdNumber + ", and  emailAddress"
				+ emailAddress + ", and  saerch cellnumber " + searchCellNumber);
		try {
			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException pce) {
			// TODO Auto-generated catch block
			pce.printStackTrace();
			logger.info("ParserConfigurationException" + pce);
		}
		InputSource src = new InputSource();
		src.setCharacterStream(new StringReader(xmlInput));

		Document doc = null;
		try {
			doc = builder.parse(src);
		} catch (SAXException saxe) {
			// TODO Auto-generated catch block
			logger.error("SAXException" + saxe);
			saxe.printStackTrace();
		} catch (IOException ioe) {
			// TODO Auto-generated catch block
			logger.info("IOException" + ioe);
			ioe.printStackTrace();
		}

		doQuickCreditAndFraudResult = doc.getElementsByTagName("doQuickCreditAndFraudResult").item(0).getTextContent();
		logger.info(" doQuickCreditAndFraudResult " + doQuickCreditAndFraudResult);

		try {
			JSONArray rootJsonArray = new JSONArray(doQuickCreditAndFraudResult);
			// CreditResult
			JSONObject creaditResultJsonObject = new JSONObject();
			creaditResultJsonObject = rootJsonArray.getJSONObject(0);

			Object rootJson = creaditResultJsonObject.get("Value");

			JSONArray creaditResultparentJsonArray = new JSONArray(rootJson.toString());
			ObjectMapper objectMapper = new ObjectMapper();

			JsonNode consumerInfoNode = objectMapper
					.readValue(creaditResultparentJsonArray.getJSONArray(0).get(0).toString(), JsonNode.class);
			JsonNode consumerInfoValue = consumerInfoNode.get("Value");
			
			logger.info("getting consumer info value");
			

			if (!consumerInfoValue.isNull() && consumerInfoValue != null && !consumerInfoValue.isEmpty()) {

				quickCreditAndFraudResponseResult = doQuickCreditFraudApiUtility
						.setCreditAndFraudInfo(consumerInfoValue, creaditResultparentJsonArray, rootJsonArray);

			} else// if no search data available in Transunion
			{
				responseMessage = "data not found";
				scoreReasons = "not found";
				scoreValue = "not found";
			}

			logger.info("getting Date Requested from json");

			JsonNode dateRequestedNode = objectMapper
					.readValue(creaditResultparentJsonArray.getJSONArray(3).get(0).toString(), JsonNode.class);
			JSONObject dateRequestedJsonObject = new JSONObject(dateRequestedNode.toString());
			dateRequestedString = dateRequestedJsonObject.get("Value").toString();
			System.out.println("dateRequestedString:" + dateRequestedString);
			System.out.println("searchCellNumber:" + searchCellNumber);

			if (searchCellNumber == "null" || searchCellNumber == null) {
				searchCellNumber = "-";
			}

			if (searchFirstName == "null" || searchFirstName.trim() == "") {
				searchFirstName = "-";
			}
			if (searchLastName == "null" || searchLastName.trim() == "") {
				searchLastName = "-";
			}

			// if it is not with bulk upload Csv
			if (isBulkCsvUploadFlag == false) {
				if (quickCreditAndFraudResponseResult.getCredit_Score() != null
						&& quickCreditAndFraudResponseResult.getScoreReasons() != null) {
					userHistoryDao.insertToUserHistoryTable("doQuickCreditAndFraud", emailAddress.trim(),
							searchFirstName.trim(), searchLastName.trim(), searchIdNumber.trim(),
							searchCellNumber.trim(), responseMessage,
							quickCreditAndFraudResponseResult.getCredit_Score().trim(),
							quickCreditAndFraudResponseResult.getScoreReasons());
				} else {
					userHistoryDao.insertToUserHistoryTable("doQuickCreditAndFraud", emailAddress.trim(),
							searchFirstName.trim(), searchLastName.trim(), searchIdNumber.trim(),
							searchCellNumber.trim(), responseMessage, "", "");
				}
				// if it is bulk csv upload
			} else {
				if (quickCreditAndFraudResponseResult.getCredit_Score() != null
						&& quickCreditAndFraudResponseResult.getScoreReasons() != null) {
					userHistoryDao.insertToUserHistoryTable("doQuickCreditAndFraud_BULK", emailAddress.trim(),
							searchFirstName.trim(), searchLastName.trim(), searchIdNumber.trim(),
							searchCellNumber.trim(), responseMessage,
							quickCreditAndFraudResponseResult.getCredit_Score().trim(),
							quickCreditAndFraudResponseResult.getScoreReasons());
				} else {
					userHistoryDao.insertToUserHistoryTable("doQuickCreditAndFraud_BULK", emailAddress.trim(),
							searchFirstName.trim(), searchLastName.trim(), searchIdNumber.trim(),
							searchCellNumber.trim(), responseMessage, "", "");
				}

			}

			if (dateRequestedString != null) {
				dateRequestedString = dateRequestedJsonObject.get("Value").toString();
			}

			logger.info(
					"saved doQuickCreditAndFraudResult json result,searchFirstName,searchLastName,searchIdNumber,dateRequestedString to database ");
			// inserting doQuickCreditAndFraudResult json
			// result,searchFirstName,searchLastName,searchIdNumber,dateRequestedString to
			// database
			tempUriDao.insertQuickCreditAndFraudResponseToDb(doQuickCreditAndFraudResult, searchFirstName,
					searchLastName, searchIdNumber, dateRequestedString);// dateRequestedString=searchDateAndTime

			logger.info(" getDoQuickCreditAndFraudResultXmmldata() method end ");

		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception" + e);
			System.out.println(e);
		}

		return quickCreditAndFraudResponseResult;
	}

	public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
		Map<String, Object> retMap = new HashMap<String, Object>();
		try {
			if (json != JSONObject.NULL) {
				retMap = toMap(json);
			}
		} catch (Exception e) {
			e.fillInStackTrace();
		}
		return retMap;
	}

	public static Map<String, Object> toMap(JSONObject object) throws JSONException {
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			Iterator<String> keysItr = object.keys();
			while (keysItr.hasNext()) {
				String key = keysItr.next();
				Object value = object.get(key);

				if (value instanceof JSONArray) {
					value = toList((JSONArray) value);
				}

				else if (value instanceof JSONObject) {
					value = toMap((JSONObject) value);
				}
				map.put(key, value);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	public static List<Object> toList(JSONArray array) throws JSONException {

		List<Object> list = new ArrayList<Object>();
		try {
			for (int i = 0; i < array.length(); i++) {
				Object value = array.get(i);
				if (value instanceof JSONArray) {
					value = toList((JSONArray) value);
				}

				else if (value instanceof JSONObject) {
					value = toMap((JSONObject) value);
				}
				list.add(value);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	/*
	 * //ConsumerInfo ObjectMapper objectMapper = new ObjectMapper(); JsonNode
	 * consumerInfoNode =
	 * objectMapper.readValue(parentJsonArray.getJSONArray(0).get(0).toString(),
	 * JsonNode.class); JsonNode consumerInfoValue = consumerInfoNode.get("Value");
	 * System.out.println(consumerInfoValue.toString());
	 * 
	 * logger.info("getting consumer info start");
	 * quickCreditAndFraudResponseResult.setRecordSeq(consumerInfoValue.get(
	 * "RecordSeq").textValue());
	 * quickCreditAndFraudResponseResult.setPart(consumerInfoValue.get("Part").
	 * textValue());
	 * quickCreditAndFraudResponseResult.setPartSeq(consumerInfoValue.get("PartSeq")
	 * .textValue());
	 * quickCreditAndFraudResponseResult.setConsumerNo(consumerInfoValue.get(
	 * "ConsumerNo").textValue());
	 * quickCreditAndFraudResponseResult.setSurname(consumerInfoValue.get("Surname")
	 * .textValue());
	 * 
	 * quickCreditAndFraudResponseResult.setForename1(consumerInfoValue.get(
	 * "Forename1").textValue());
	 * 
	 * quickCreditAndFraudResponseResult.setForename2(consumerInfoValue.get(
	 * "Forename2").textValue());
	 * 
	 * quickCreditAndFraudResponseResult.setForename3(consumerInfoValue.get(
	 * "Forename3").textValue());
	 * 
	 * quickCreditAndFraudResponseResult.setTitle(consumerInfoValue.get("Title").
	 * textValue());
	 * 
	 * quickCreditAndFraudResponseResult.setGender(consumerInfoValue.get("Gender").
	 * textValue());
	 * 
	 * quickCreditAndFraudResponseResult.setNameInfoDate(consumerInfoValue.get(
	 * "NameInfoDate").textValue());
	 * 
	 * quickCreditAndFraudResponseResult.setDateOfBirth(consumerInfoValue.get(
	 * "DateOfBirth").textValue());
	 * 
	 * quickCreditAndFraudResponseResult.setIdentityNo1(consumerInfoValue.get(
	 * "IdentityNo1").textValue());
	 * 
	 * quickCreditAndFraudResponseResult.setIdentityNo2(consumerInfoValue.get(
	 * "IdentityNo2").textValue());
	 * 
	 * quickCreditAndFraudResponseResult
	 * .setMaritalStatusCode(consumerInfoValue.get("MaritalStatusCode").textValue())
	 * ;
	 * 
	 * quickCreditAndFraudResponseResult
	 * .setMaritalStatusDesc(consumerInfoValue.get("MaritalStatusDesc").textValue())
	 * ; quickCreditAndFraudResponseResult.setDependants(consumerInfoValue.get(
	 * "Dependants").textValue());
	 * quickCreditAndFraudResponseResult.setSpouseName1(consumerInfoValue.get(
	 * "SpouseName1").textValue());
	 * quickCreditAndFraudResponseResult.setSpouseName2(consumerInfoValue.get(
	 * "SpouseName2").textValue());
	 * quickCreditAndFraudResponseResult.setTelephoneNumbers(consumerInfoValue.get(
	 * "TelephoneNumbers").textValue());
	 * quickCreditAndFraudResponseResult.setDeceasedDate(consumerInfoValue.get(
	 * "DeceasedDate").textValue());
	 * quickCreditAndFraudResponseResult.setCellNumber(consumerInfoValue.get(
	 * "CellNumber").textValue());
	 * quickCreditAndFraudResponseResult.seteMail(consumerInfoValue.get("EMail").
	 * textValue()); logger.info("getting consumer info end");
	 * 
	 * logger.info("getting LastAddress start"); //LastAddress JsonNode
	 * lastAddressNode =
	 * objectMapper.readValue(parentJsonArray.getJSONArray(0).get(1).toString(),
	 * JsonNode.class); JsonNode lastAddressValue = lastAddressNode.get("Value");
	 * quickCreditAndFraudResponseResult.setAddConsumerNo(lastAddressValue.get(
	 * "ConsumerNo").textValue());
	 * quickCreditAndFraudResponseResult.setInformationDate(lastAddressValue.get(
	 * "InformationDate").textValue());
	 * quickCreditAndFraudResponseResult.setLine1(lastAddressValue.get("Line1").
	 * textValue());
	 * quickCreditAndFraudResponseResult.setLine2(lastAddressValue.get("Line2").
	 * textValue());
	 * quickCreditAndFraudResponseResult.setSuburb(lastAddressValue.get("Suburb").
	 * textValue());
	 * quickCreditAndFraudResponseResult.setCity(lastAddressValue.get("City").
	 * textValue());
	 * quickCreditAndFraudResponseResult.setPostalCode(lastAddressValue.get(
	 * "PostalCode").textValue());
	 * quickCreditAndFraudResponseResult.setProvinceCode(lastAddressValue.get(
	 * "ProvinceCode").textValue());
	 * quickCreditAndFraudResponseResult.setProvince(lastAddressValue.get("Province"
	 * ).textValue());
	 * quickCreditAndFraudResponseResult.setAddressPeriod(lastAddressValue.get(
	 * "AddressPeriod").textValue());
	 * quickCreditAndFraudResponseResult.setOwnerTenant(lastAddressValue.get(
	 * "OwnerTenant").textValue());
	 * quickCreditAndFraudResponseResult.setAddressChanged(lastAddressValue.get(
	 * "AddressChanged").textValue()); logger.info("getting LastAddress end");
	 * 
	 * 
	 * //CreditScore logger.info("getting CreditScore start"); JsonNode
	 * creaditScoreNode =
	 * objectMapper.readValue(parentJsonArray.getJSONArray(1).get(0).toString(),
	 * JsonNode.class); JsonNode creaditScoreValue = creaditScoreNode.get("Value");
	 * quickCreditAndFraudResponseResult.setCreaditScoreConsumerNo(creaditScoreValue
	 * .get("ConsumerNo").intValue());
	 * 
	 * 
	 * logger.info("getting CreditScore end");
	 * 
	 * //FraudPermission logger.info("getting FraudPermission start"); JsonNode
	 * fraudPermissionNode =
	 * objectMapper.readValue(parentJsonArray.getJSONArray(2).get(0).toString(),
	 * JsonNode.class); JSONObject fraudPermissionJsonObject=new
	 * JSONObject(fraudPermissionNode.toString());
	 * quickCreditAndFraudResponseResult.setFraudPermission(
	 * fraudPermissionJsonObject.get("Value").toString());
	 * logger.info("getting FraudPermission end");
	 * 
	 * //Date Requested logger.info("getting Date Requested start"); JsonNode
	 * dateRequestedNode =
	 * objectMapper.readValue(parentJsonArray.getJSONArray(3).get(0).toString(),
	 * JsonNode.class); JSONObject dateRequestedJsonObject=new
	 * JSONObject(dateRequestedNode.toString());
	 * quickCreditAndFraudResponseResult.setDateRequested(dateRequestedJsonObject.
	 * get("Value").toString()); logger.info("getting Date Requested end");
	 * 
	 * //user Email logger.info("getting user Email start"); JsonNode userEmailNode
	 * = objectMapper.readValue(parentJsonArray.getJSONArray(4).get(0).toString(),
	 * JsonNode.class); JSONObject userEmailJsonObject=new
	 * JSONObject(userEmailNode.toString());
	 * quickCreditAndFraudResponseResult.setUserEmail(userEmailJsonObject.get(
	 * "Value").toString()); logger.info("getting user Email end");
	 * 
	 * //AuditLog logger.info("getting AuditLog start"); JsonNode auditLogNode =
	 * objectMapper.readValue(parentJsonArray.getJSONArray(5).get(0).toString(),
	 * JsonNode.class); JSONObject auditLogJsonObject=new
	 * JSONObject(auditLogNode.toString());
	 * quickCreditAndFraudResponseResult.setAuditLog(auditLogJsonObject.get("Value")
	 * .toString()); logger.info("getting AuditLog end");
	 */

}
