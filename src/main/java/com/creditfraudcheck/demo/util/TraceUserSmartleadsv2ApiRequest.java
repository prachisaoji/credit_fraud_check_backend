package com.creditfraudcheck.demo.util;

import java.io.EOFException;
import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.google.gson.Gson;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@Component
public class TraceUserSmartleadsv2ApiRequest {
	@Autowired
	Util util;
	Logger logger = Logger.getLogger(TraceUserSmartleadsv2ApiRequest.class.getName());

	public String traceUserSmartleadsv2ApiRequest(String lastName, String cellNumber)
			throws EOFException, ClassNotFoundException, IOException {

		Properties prop = util.readPropertiesFile("traceUserSmartleadsv2Api.properties");
		Gson gsonUtiltiy = new Gson();
		String xmlResponse = "";
		DocumentBuilder builder = null;
		String traceUserSmartleadsv2ApiJsonResult = "";
		String apiUrl = prop.getProperty("apiUrl");

		logger.info("In traceUserSmartleadsv2ApiRequest() method for  lastName : " + lastName + " and for cell Number"
				+ cellNumber);

		String soap_string = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
				+ "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
				+ "<soap:Body>" + "<traceUser xmlns=\"http://tempuri.org/\">" + "<apikey>" + prop.getProperty("apiKey")
				+ "</apikey>" + "<username>" + prop.getProperty("username") + "</username>" + "<password>"
				+ prop.getProperty("password") + "</password>" + "<customer_lastname>" + lastName
				+ "</customer_lastname>" + " <customer_cell>" + cellNumber + "</customer_cell>" + "	</traceUser>"
				+ "</soap:Body>" + "</soap:Envelope>";

		System.out.println(soap_string);
		OkHttpClient client = new OkHttpClient();

		MediaType mediaType = MediaType.parse("text/xml");

		RequestBody body = RequestBody.create(mediaType, soap_string);
		Request requestForPartSearch = new Request.Builder().url(apiUrl).post(body)
				.addHeader("content-type", "text/xml").build();

		Response response = null;

		try {
			response = client.newCall(requestForPartSearch).execute();
			System.out.println(gsonUtiltiy.toJson(" Response : " + response));
			logger.info("TraceUserSmartleadsv2Api response get successfully");
			xmlResponse = response.body().string(); // <-- The XML SOAP response
			// parse xml
			try {
				builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			} catch (ParserConfigurationException pce) {
				// TODO Auto-generated catch block
				pce.printStackTrace();
				logger.info("ParserConfigurationException" + pce);
			}
			InputSource src = new InputSource();
			src.setCharacterStream(new StringReader(xmlResponse));

			Document doc = null;
			try {
				doc = builder.parse(src);
			} catch (SAXException saxe) {
				// TODO Auto-generated catch block
				logger.info("SAXException" + saxe);
				saxe.printStackTrace();
			} catch (IOException ioe) {
				// TODO Auto-generated catch block
				logger.info("IOException" + ioe);
				ioe.printStackTrace();
			}

			traceUserSmartleadsv2ApiJsonResult = doc.getElementsByTagName("traceUserResult").item(0).getTextContent();
			// if the data not available in TU database

			if (traceUserSmartleadsv2ApiJsonResult.contains("R0109")) {
				traceUserSmartleadsv2ApiJsonResult = "";
			} else {

				traceUserSmartleadsv2ApiJsonResult = doc.getElementsByTagName("traceUserResult").item(0)
						.getTextContent();
			}
			logger.info("traceUserSmartleadsv2ApiRequest() method end");

		} catch (EOFException eof) {
			eof.printStackTrace();
			logger.info("EOFException" + eof);
			System.out.println(eof);
		} catch (IOException ie) {
			ie.printStackTrace();
			logger.info("IOException" + ie);
			System.out.println(ie);
		}

		catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception" + e);

			System.out.println(e);
		}
		return traceUserSmartleadsv2ApiJsonResult;
	}

	public String[] parsTraceUserSmartleadsv2ApiResponse(String traceUserSmartleadsv2ApiJsonResult) {
		String traceUserSmartleadsv2ApiResponseValues[] = new String[3];
		logger.info("In parsTraceUserSmartleadsv2ApiResponse() method");
		String lastName = "";
		String firstName = "";
		String idNumber = "";

		JSONArray jsonArray = null;
		try {
			logger.info(" traceUserSmartleadsv2ApiResult " + traceUserSmartleadsv2ApiJsonResult);
			try {
				jsonArray = new JSONArray(traceUserSmartleadsv2ApiJsonResult);
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e.getMessage());

			}

			for (Object o : jsonArray) {
				JSONObject jsonLineItem = (JSONObject) o;
				lastName = jsonLineItem.getString("Surname");
				firstName = jsonLineItem.getString("Forename1");
				idNumber = jsonLineItem.getString("IdentityNumber1");

			}
			logger.info(" getting lastName,firstName,idNumber from  traceUserSmartleadsv2ApiResult are " + "lastName: "
					+ lastName + " firstName: " + firstName + " idNumber: " + idNumber);

			traceUserSmartleadsv2ApiResponseValues[0] = lastName;
			traceUserSmartleadsv2ApiResponseValues[1] = firstName;
			traceUserSmartleadsv2ApiResponseValues[2] = idNumber;
			logger.info("In parsTraceUserSmartleadsv2ApiResponse() method end");

		}

		catch (Exception e) {

			e.printStackTrace();
			logger.error(e.getMessage());
		}

		return traceUserSmartleadsv2ApiResponseValues;
	}

}
