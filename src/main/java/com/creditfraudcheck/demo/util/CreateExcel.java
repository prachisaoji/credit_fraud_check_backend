package com.creditfraudcheck.demo.util;

import java.io.EOFException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

@Component
public class CreateExcel {
	Logger logger = Logger.getLogger(CreateExcel.class.getName());

	// create excel sheet from the Result set
	public void exportToExcelFile(ResultSet rs, String fileName)
			throws EOFException, ClassNotFoundException, IOException {

		logger.info(" In exportToExcelFile()  method");

		// get excel file path
		Util util = new Util();
		Properties prop = util.readPropertiesFile("application.properties");
		String excelFilePath = prop.getProperty("excelFilePath");

		logger.info("Excel file path is :" + excelFilePath + " Excel file name is " + fileName);

		String excelFileName = excelFilePath + fileName;

		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("credit_score_report");

		try {
			// write header lines to excel
			writeHeaderLine(sheet);

			// write data to a excel
			writeDataLines(rs, workbook, sheet);

			FileOutputStream outputStream = new FileOutputStream(excelFileName);
			workbook.write(outputStream);

			logger.info("create excel sheet with filepath  : " + excelFileName);

			workbook.close();
		}

		catch (SQLException e) {
			System.out.println("Datababse error:");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("File IO error:");
			e.printStackTrace();
		}

	}

	public void writeHeaderLine(XSSFSheet sheet) {

		logger.info(" In writeHeaderLine()  method");

		Row headerRow = sheet.createRow(0);

		Cell headerCell = headerRow.createCell(0);
		headerCell.setCellValue("Api Service");

		headerCell = headerRow.createCell(1);
		headerCell.setCellValue("Date");

		headerCell = headerRow.createCell(2);
		headerCell.setCellValue("User Name");

		headerCell = headerRow.createCell(3);
		headerCell.setCellValue("First Name");

		headerCell = headerRow.createCell(4);
		headerCell.setCellValue("Last Name");

		headerCell = headerRow.createCell(5);
		headerCell.setCellValue("Id number");

		headerCell = headerRow.createCell(6);
		headerCell.setCellValue("Cell number");

		headerCell = headerRow.createCell(7);
		headerCell.setCellValue("Response Status");

		headerCell = headerRow.createCell(8);
		headerCell.setCellValue("Score");

		headerCell = headerRow.createCell(9);
		headerCell.setCellValue("Score Reasons");

	}

	private void writeDataLines(ResultSet rs, XSSFWorkbook workbook, XSSFSheet sheet) throws SQLException {

		logger.info(" In writeDataLines()  method");

		int rowCount = 1;
		while (rs.next()) {

			Row row = sheet.createRow(rowCount++);
			int columnCount = 0;

			Cell cell = row.createCell(columnCount++);
			cell.setCellValue(rs.getString("api_service"));

			cell = row.createCell(columnCount++);
			cell.setCellValue(rs.getString("date"));

			cell = row.createCell(columnCount++);
			cell.setCellValue(rs.getString("user_name"));

			cell = row.createCell(columnCount++);
			cell.setCellValue(rs.getString("search_firstname"));

			cell = row.createCell(columnCount++);
			cell.setCellValue(rs.getString("search_lastname"));

			cell = row.createCell(columnCount++);
			cell.setCellValue(rs.getString("search_idnumber"));

			cell = row.createCell(columnCount++);
			cell.setCellValue(rs.getString("search_cellnumber"));

			cell = row.createCell(columnCount++);
			cell.setCellValue(rs.getString("response_status"));

			cell = row.createCell(columnCount++);
			cell.setCellValue(rs.getString("creadit_score"));

			cell = row.createCell(columnCount++);
			cell.setCellValue(rs.getString("score_reasons"));

		}
	}
}
