package com.creditfraudcheck.demo.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.creditfraudcheck.demo.model.BulkCsvModel;

@Component
public class ReadCsvFile {
	
	static Logger logger = Logger.getLogger(ReadCsvFile.class.getClass());
	
	public static List<BulkCsvModel> readCsvFile(ByteArrayInputStream inputFilestream ) throws FileNotFoundException {
		List<BulkCsvModel> bulkCsvModelList = new ArrayList<BulkCsvModel>();
		
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputFilestream ));
		String line;
		String surName = "";
		String foreName1 = "";
		String idNumber = "";
		String cellNumber = "";
		int iteration = 0;
	
		
		logger.info("In readCsvFile() ");

		try {
			while ((line = bufferedReader.readLine()) != null) {

				// String[] temp = line.split("\\s*,\\s*");

				
				if(iteration == 0) {
			        iteration++;  
			        continue;
				}
				String[] temp = line.split(",", -1);

				if (temp[0] != null) {
					surName = temp[0];
				}

				if (temp[1] != null) {
					foreName1 = temp[1];
				}

				if (temp[2] != null) {
					idNumber = temp[2];
				}

				if (temp[3] != null) {
					cellNumber = temp[3];

				}

				
				try {
					bulkCsvModelList.add(new BulkCsvModel(surName, foreName1, idNumber, cellNumber));
				} catch (Exception e) {
					e.printStackTrace();
				}


			}
			
		} catch (NumberFormatException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			bufferedReader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return bulkCsvModelList;
	}

}
