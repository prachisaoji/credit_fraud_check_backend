package com.creditfraudcheck.demo.util;

import java.sql.SQLException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.creditfraudcheck.demo.dao.QuickCreditAndFraudResponseDao;
import com.creditfraudcheck.demo.model.QuickCreditAndFraudResponseResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

@Component
public class DoQuickCreditFraudApiUtility {

	@Autowired
	QuickCreditAndFraudResponseDao tempUriDao;
	Logger logger = Logger.getLogger(DoQuickCreditFraudApiUtility.class.getName());
	Gson gson = new Gson();

	QuickCreditAndFraudResponseResult setCreditAndFraudInfo(JsonNode consumerInfoValue,
			JSONArray creaditResultparentJsonArray, JSONArray rootJsonArray)
			throws JsonMappingException, JsonProcessingException, JSONException, SQLException {
		String informationDate = "";
		String[] reasonCodeArray = new String[3];

		String rating = "";
		String ratingDescription = "";
		String fraudReasonCode = "";
		String fraudReasonDescription = "";
		String dateOfBirth = "";
		ObjectMapper objectMapper = new ObjectMapper();
		QuickCreditAndFraudResponseResult quickCreditAndFraudResponseResult = new QuickCreditAndFraudResponseResult();
		logger.info(" In setCreditAndFraudInfo() method");

		try {
			logger.info(" CreditResult start");
			logger.info("getting consumer info start");
			quickCreditAndFraudResponseResult.setForename1(consumerInfoValue.get("Forename1").textValue());
			quickCreditAndFraudResponseResult.setForename2(consumerInfoValue.get("Forename2").textValue());
			quickCreditAndFraudResponseResult.setSurname(consumerInfoValue.get("Surname").textValue());
			// parse date
			dateOfBirth = consumerInfoValue.get("DateOfBirth").textValue();
			if (dateOfBirth.length() == 8 && dateOfBirth.matches("[0-9]+")) {
				dateOfBirth = Util.convertDateIntoFormatedDate(dateOfBirth);
			}

			quickCreditAndFraudResponseResult.setDateOfBirth(dateOfBirth);
			quickCreditAndFraudResponseResult
					.setTelephoneNumbers(consumerInfoValue.get("TelephoneNumbers").textValue());
			quickCreditAndFraudResponseResult.setTitle(consumerInfoValue.get("Title").textValue());
			quickCreditAndFraudResponseResult.setGender(consumerInfoValue.get("Gender").textValue());
			quickCreditAndFraudResponseResult.setIdentityNo1(consumerInfoValue.get("IdentityNo1").textValue());
			quickCreditAndFraudResponseResult
					.setMaritalStatusDesc(consumerInfoValue.get("MaritalStatusDesc").textValue());
			quickCreditAndFraudResponseResult.setDeceasedDate(consumerInfoValue.get("DeceasedDate").textValue());
			logger.info("getting consumer info end");
			// CreditScore
			logger.info("getting CreditScore info start");
			JsonNode creditScoreNode = objectMapper
					.readValue(creaditResultparentJsonArray.getJSONArray(1).get(0).toString(), JsonNode.class);
			JsonNode creditScoreValue = creditScoreNode.get("Value");
			quickCreditAndFraudResponseResult.setCredit_Score(creditScoreValue.get("Credit_Score").textValue());
			String reason1 = creditScoreValue.get("Reason1").textValue();
			String reason2 = creditScoreValue.get("Reason2").textValue();
			String reason3 = creditScoreValue.get("Reason3").textValue();

			System.out.println("creditScore " + creditScoreValue.get("Credit_Score").textValue() + "reason1 " + reason1
					+ " reason2 " + reason2 + " reason3" + reason3);
			reasonCodeArray[0] = reason1;
			reasonCodeArray[1] = reason2;
			reasonCodeArray[2] = reason3;
			Map<String, String> reasonCodeAndValueMap = tempUriDao.getReasonCodeAndValue(reasonCodeArray);
			quickCreditAndFraudResponseResult.setReasonCodeAndValueMap(reasonCodeAndValueMap);
			
			quickCreditAndFraudResponseResult.setScoreReasons(String.join(",", reasonCodeArray));
			logger.info("getting CreditScore info end");

			// LastAddress
			logger.info("getting LastAddress of CreditResult start");

			JsonNode lastAddressNode = objectMapper
					.readValue(creaditResultparentJsonArray.getJSONArray(0).get(1).toString(), JsonNode.class);
			JsonNode lastAddressValue = lastAddressNode.get("Value");
			quickCreditAndFraudResponseResult.setLine1(lastAddressValue.get("Line1").textValue());
			quickCreditAndFraudResponseResult.setLine2(lastAddressValue.get("Line2").textValue());

			quickCreditAndFraudResponseResult.setSuburb(lastAddressValue.get("Suburb").textValue());
			quickCreditAndFraudResponseResult.setCity(lastAddressValue.get("City").textValue());
			quickCreditAndFraudResponseResult.setPostalCode(lastAddressValue.get("ProvinceCode").textValue());
			informationDate = lastAddressValue.get("InformationDate").textValue();
			if (informationDate.length() == 8 && informationDate.matches("[0-9]+")) {
				informationDate = Util.convertDateIntoFormatedDate(informationDate);
			}

			quickCreditAndFraudResponseResult.setInformationDate(informationDate);

			logger.info("getting LastAddress of CreditResult ");

			logger.info(" CreditResult end");

			// FraudResult
			logger.info(" FraudResult start");
			JSONObject fraudResultJsonObject = new JSONObject();
			fraudResultJsonObject = rootJsonArray.getJSONObject(1);

			Object fraudResultRootJson = fraudResultJsonObject.get("Value");

			JSONArray fraudResultParentJsonArray = new JSONArray(fraudResultRootJson.toString());
			JsonNode fraudConsumerInfoNode = objectMapper
					.readValue(fraudResultParentJsonArray.getJSONArray(0).get(0).toString(), JsonNode.class);
			JsonNode fraudConsumerInfoValue = fraudConsumerInfoNode.get("Value");

			if (!fraudConsumerInfoValue.isNull() && fraudConsumerInfoValue != null
					&& !fraudConsumerInfoValue.isEmpty()) {

				JsonNode fraudInfoNode = objectMapper
						.readValue(fraudResultParentJsonArray.getJSONArray(1).get(0).toString(), JsonNode.class);
				JSONObject fraudInfoJsonObject = new JSONObject(fraudInfoNode.toString());
				JSONObject fraudInfoJsonResponse = (JSONObject) fraudInfoJsonObject.get("Value");

				rating = fraudInfoJsonResponse.get("Rating").toString();

				logger.info("Rating get FraudResult :" + rating);

				ratingDescription = fraudInfoJsonResponse.get("RatingDescription").toString();
				logger.info("RatingDescription  get FraudResult: " + ratingDescription);

				fraudReasonCode = fraudInfoJsonResponse.get("ReasonCode").toString();

				logger.info("fraudReasonCode  get FraudResult: " + fraudReasonCode);

				fraudReasonDescription = fraudInfoJsonResponse.get("ReasonDescription").toString();
				logger.info("fraudReasonDescription  get FraudResult: " + fraudReasonDescription);

				quickCreditAndFraudResponseResult.setFraudReasonCode(fraudReasonCode.toString().replaceAll("\"", "")
						.replace(",", "").replace("[", "").replace("]", "").trim());
				quickCreditAndFraudResponseResult.setFraudReasonDescription(fraudReasonDescription.toString()
						.replaceAll("\"", "").replace(",", "").replace("[", "").replace("]", "").trim());
				quickCreditAndFraudResponseResult.setRating(rating);
				quickCreditAndFraudResponseResult.setRatingDescription(ratingDescription);

				logger.info(" FraudResult end");
				logger.info(" In setCreditAndFraudInfo() method end");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception" + e);
			System.out.println(e);
		}

		return quickCreditAndFraudResponseResult;
	}

	/*QuickCreditAndFraudResponseResult setCreditAndFraudInfo(JsonNode consumerInfoValue,
			JSONArray creaditResultparentJsonArray, JSONArray rootJsonArray)
			throws JsonMappingException, JsonProcessingException, JSONException, SQLException {
		String scoreReasons = "";
		String scoreValue = "";
		String informationDate = "";
		String[] reasonCodeArray = null;
		String indicators = "";
		String rating = "";
		String ratingDescription = "";
		String fraudReasonCode = "";
		String fraudReasonDescription = "";
		String dateOfBirth = "";
		ObjectMapper objectMapper = new ObjectMapper();
		QuickCreditAndFraudResponseResult quickCreditAndFraudResponseResult = new QuickCreditAndFraudResponseResult();
		logger.info(" In setCreditAndFraudInfo() method");

		try {
			logger.info(" CreditResult start");
			logger.info("getting consumer info start");
			quickCreditAndFraudResponseResult.setForename1(consumerInfoValue.get("Forename1").textValue());
			quickCreditAndFraudResponseResult.setForename2(consumerInfoValue.get("Forename2").textValue());
			quickCreditAndFraudResponseResult.setSurname(consumerInfoValue.get("Surname").textValue());
			// parse date
			dateOfBirth = consumerInfoValue.get("DateOfBirth").textValue();
			if (dateOfBirth.length() == 8 && dateOfBirth.matches("[0-9]+")) {
				dateOfBirth = Util.convertDateIntoFormatedDate(dateOfBirth);
			}

			quickCreditAndFraudResponseResult.setDateOfBirth(dateOfBirth);
			quickCreditAndFraudResponseResult
					.setTelephoneNumbers(consumerInfoValue.get("TelephoneNumbers").textValue());
			quickCreditAndFraudResponseResult.setTitle(consumerInfoValue.get("Title").textValue());
			quickCreditAndFraudResponseResult.setGender(consumerInfoValue.get("Gender").textValue());
			quickCreditAndFraudResponseResult.setIdentityNo1(consumerInfoValue.get("IdentityNo1").textValue());
			quickCreditAndFraudResponseResult
					.setMaritalStatusDesc(consumerInfoValue.get("MaritalStatusDesc").textValue());
			quickCreditAndFraudResponseResult.setDeceasedDate(consumerInfoValue.get("DeceasedDate").textValue());
			logger.info("getting consumer info end");
			// LastAddress
			logger.info("getting LastAddress of CreditResult start");

			JsonNode lastAddressNode = objectMapper
					.readValue(creaditResultparentJsonArray.getJSONArray(0).get(1).toString(), JsonNode.class);
			JsonNode lastAddressValue = lastAddressNode.get("Value");
			quickCreditAndFraudResponseResult.setLine1(lastAddressValue.get("Line1").textValue());
			quickCreditAndFraudResponseResult.setLine2(lastAddressValue.get("Line2").textValue());

			quickCreditAndFraudResponseResult.setSuburb(lastAddressValue.get("Suburb").textValue());
			quickCreditAndFraudResponseResult.setCity(lastAddressValue.get("City").textValue());
			quickCreditAndFraudResponseResult.setPostalCode(lastAddressValue.get("ProvinceCode").textValue());
			informationDate = lastAddressValue.get("InformationDate").textValue();
			if (informationDate.length() == 8 && dateOfBirth.matches("[0-9]+")) {
				informationDate = Util.convertDateIntoFormatedDate(dateOfBirth);
			}

			quickCreditAndFraudResponseResult.setInformationDate(informationDate);

			logger.info("getting LastAddress of CreditResult ");

			logger.info("getting CreditScore start");

			JsonNode creditScoreNode = objectMapper
					.readValue(creaditResultparentJsonArray.getJSONArray(1).get(0).toString(), JsonNode.class);
			JSONObject creditScoreJsonObject = new JSONObject(creditScoreNode.toString());
			JSONObject creditScoreJsonResponse = (JSONObject) creditScoreJsonObject.get("Value");

			scoreReasons = creditScoreJsonResponse.get("ScoreReasons").toString();
			scoreReasons = scoreReasons.replace("\"", "");
			quickCreditAndFraudResponseResult.setScoreReasons(scoreReasons);
			
			reasonCodeArray = scoreReasons.split(",");
			logger.info("Getting scoreReasons from CreditScore json are : " + scoreReasons.toString());

			Map<String, String> reasonCodeAndValueMap = tempUriDao.getReasonCodeAndValue(reasonCodeArray);
			quickCreditAndFraudResponseResult.setReasonCodeAndValueMap(reasonCodeAndValueMap);

			indicators = creditScoreJsonResponse.get("Indicators").toString();
			logger.info("getting Indicators from CreditScore json are :" + indicators.toString());

			JSONArray indicatorsJsonArray = (JSONArray) creditScoreJsonResponse.get("Indicators");
			JSONObject indicatorsJsonObject = indicatorsJsonArray.getJSONObject(0);
			scoreValue = indicatorsJsonObject.getString("Score");
			quickCreditAndFraudResponseResult.setCredit_Score(scoreValue);
			logger.info("getting scoreValue Indicators json arry are :" + scoreValue.toString());
			logger.info("getting CreditScore end");

			logger.info(" CreditResult end");

			// FraudResult
			logger.info(" FraudResult start");
			JSONObject fraudResultJsonObject = new JSONObject();
			fraudResultJsonObject = rootJsonArray.getJSONObject(1);

			Object fraudResultRootJson = fraudResultJsonObject.get("Value");

			JSONArray fraudResultParentJsonArray = new JSONArray(fraudResultRootJson.toString());
			JsonNode fraudConsumerInfoNode = objectMapper
					.readValue(fraudResultParentJsonArray.getJSONArray(0).get(0).toString(), JsonNode.class);
			JsonNode fraudConsumerInfoValue = fraudConsumerInfoNode.get("Value");

			if (!fraudConsumerInfoValue.isNull() && fraudConsumerInfoValue != null
					&& !fraudConsumerInfoValue.isEmpty()) {

				JsonNode fraudInfoNode = objectMapper
						.readValue(fraudResultParentJsonArray.getJSONArray(1).get(0).toString(), JsonNode.class);
				JSONObject fraudInfoJsonObject = new JSONObject(fraudInfoNode.toString());
				JSONObject fraudInfoJsonResponse = (JSONObject) fraudInfoJsonObject.get("Value");

				rating = fraudInfoJsonResponse.get("Rating").toString();

				logger.info("Rating get FraudResult :" + rating);

				ratingDescription = fraudInfoJsonResponse.get("RatingDescription").toString();
				logger.info("RatingDescription  get FraudResult: " + ratingDescription);

				fraudReasonCode = fraudInfoJsonResponse.get("ReasonCode").toString();

				logger.info("fraudReasonCode  get FraudResult: " + fraudReasonCode);

				fraudReasonDescription = fraudInfoJsonResponse.get("ReasonDescription").toString();
				logger.info("fraudReasonDescription  get FraudResult: " + fraudReasonDescription);

				quickCreditAndFraudResponseResult.setFraudReasonCode(fraudReasonCode.toString().replaceAll("\"", "")
						.replace(",", "").replace("[", "").replace("]", "").trim());
				quickCreditAndFraudResponseResult.setFraudReasonDescription(fraudReasonDescription.toString()
						.replaceAll("\"", "").replace(",", "").replace("[", "").replace("]", "").trim());
				quickCreditAndFraudResponseResult.setRating(rating);
				quickCreditAndFraudResponseResult.setRatingDescription(ratingDescription);

				logger.info(" FraudResult end");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception" + e);
			System.out.println(e);
		}

		return quickCreditAndFraudResponseResult;
	}
*/
	

}
