package com.creditfraudcheck.demo.model;

import java.util.Map;

public class QuickCreditAndFraudResponseResult {

	private String consumerNo;
	private String surname;
	private String forename1;
	private String forename2;
	private String forename3;
	private String title;
	private String gender;
	private String nameInfoDate;
	private String dateOfBirth;
	private String identityNo1;
	private String identityNo2;
	private String maritalStatusCode;
	private String maritalStatusDesc;
	private String dependants;
	private String spouseName1;
	private String spouseName2;
	private String telephoneNumbers;
	private String deceasedDate;
	private String cellNumber;
	private String eMail;

	// LastAddress
	private String addConsumerNo;
	private String InformationDate;
	private String Line1;
	private String Line2;
	private String Suburb;
	private String City;
	private String PostalCode;
	private String ProvinceCode;
	private String Province;
	private String AddressPeriod;
	private String OwnerTenant;
	private String AddressChanged;

	// CreditScore
	private int creaditScoreConsumerNo;
	private int scoreInd;
	private String credit_Score;
	private String Band;
	private String Percentile;

	private String scoreReasons;

	private String FraudPermission;
	private String dateRequested;
	private String userEmail;
	private String AuditLog;

	// FraudResult
	private String Rating;
	private String RatingDescription;
	private Map<String, String> reasonCodeAndValueMap;
	private String fraudReasonCode;
	private String fraudReasonDescription;
	
	private boolean isEmailPasswordValid;
	private String userAuthenticationFailedMsg;
	private String responseResultErrorMsg;

	public String getConsumerNo() {
		return consumerNo;
	}

	public void setConsumerNo(String consumerNo) {
		this.consumerNo = consumerNo;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getForename1() {
		return forename1;
	}

	public void setForename1(String forename1) {
		this.forename1 = forename1;
	}

	public String getForename2() {
		return forename2;
	}

	public void setForename2(String forename2) {
		this.forename2 = forename2;
	}

	public String getForename3() {
		return forename3;
	}

	public void setForename3(String forename3) {
		this.forename3 = forename3;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getNameInfoDate() {
		return nameInfoDate;
	}

	public void setNameInfoDate(String nameInfoDate) {
		this.nameInfoDate = nameInfoDate;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getIdentityNo1() {
		return identityNo1;
	}

	public void setIdentityNo1(String identityNo1) {
		this.identityNo1 = identityNo1;
	}

	public String getIdentityNo2() {
		return identityNo2;
	}

	public void setIdentityNo2(String identityNo2) {
		this.identityNo2 = identityNo2;
	}

	public String getMaritalStatusCode() {
		return maritalStatusCode;
	}

	public void setMaritalStatusCode(String maritalStatusCode) {
		this.maritalStatusCode = maritalStatusCode;
	}

	public String getMaritalStatusDesc() {
		return maritalStatusDesc;
	}

	public void setMaritalStatusDesc(String maritalStatusDesc) {
		this.maritalStatusDesc = maritalStatusDesc;
	}

	public String getDependants() {
		return dependants;
	}

	public String getFraudReasonCode() {
		return fraudReasonCode;
	}

	public void setFraudReasonCode(String fraudReasonCode) {
		this.fraudReasonCode = fraudReasonCode;
	}

	public String getFraudReasonDescription() {
		return fraudReasonDescription;
	}

	public void setFraudReasonDescription(String fraudReasonDescription) {
		this.fraudReasonDescription = fraudReasonDescription;
	}

	public void setDependants(String dependants) {
		this.dependants = dependants;
	}

	public String getSpouseName1() {
		return spouseName1;
	}

	public String getRatingDescription() {
		return RatingDescription;
	}

	public void setRatingDescription(String ratingDescription) {
		RatingDescription = ratingDescription;
	}

	public void setSpouseName1(String spouseName1) {
		this.spouseName1 = spouseName1;
	}

	public String getSpouseName2() {
		return spouseName2;
	}

	public void setSpouseName2(String spouseName2) {
		this.spouseName2 = spouseName2;
	}

	public String getTelephoneNumbers() {
		return telephoneNumbers;
	}

	public void setTelephoneNumbers(String telephoneNumbers) {
		this.telephoneNumbers = telephoneNumbers;
	}

	public String getDeceasedDate() {
		return deceasedDate;
	}

	public void setDeceasedDate(String deceasedDate) {
		this.deceasedDate = deceasedDate;
	}

	public String getCellNumber() {
		return cellNumber;
	}

	public void setCellNumber(String cellNumber) {
		this.cellNumber = cellNumber;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}
	// LastAddress

	public String getAddConsumerNo() {
		return addConsumerNo;
	}

	public void setAddConsumerNo(String addConsumerNo) {
		this.addConsumerNo = addConsumerNo;
	}

	public String getInformationDate() {
		return InformationDate;
	}

	public void setInformationDate(String informationDate) {
		InformationDate = informationDate;
	}

	public String getLine1() {
		return Line1;
	}

	public void setLine1(String line1) {
		Line1 = line1;
	}

	public String getLine2() {
		return Line2;
	}

	public void setLine2(String line2) {
		Line2 = line2;
	}

	public String getSuburb() {
		return Suburb;
	}

	public void setSuburb(String suburb) {
		Suburb = suburb;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getPostalCode() {
		return PostalCode;
	}

	public void setPostalCode(String postalCode) {
		PostalCode = postalCode;
	}

	public String getProvinceCode() {
		return ProvinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		ProvinceCode = provinceCode;
	}

	public String getProvince() {
		return Province;
	}

	public void setProvince(String province) {
		Province = province;
	}

	public String getAddressPeriod() {
		return AddressPeriod;
	}

	public void setAddressPeriod(String addressPeriod) {
		AddressPeriod = addressPeriod;
	}

	public String getOwnerTenant() {
		return OwnerTenant;
	}

	public void setOwnerTenant(String ownerTenant) {
		OwnerTenant = ownerTenant;
	}

	public String getAddressChanged() {
		return AddressChanged;
	}

	public void setAddressChanged(String addressChanged) {
		AddressChanged = addressChanged;
	}

	// CreditScore

	public int getScoreInd() {
		return scoreInd;
	}

	public void setScoreInd(int scoreInd) {
		this.scoreInd = scoreInd;
	}

	public String getCredit_Score() {
		return credit_Score;
	}

	public void setCredit_Score(String credit_Score) {
		this.credit_Score = credit_Score;
	}

	public String getBand() {
		return Band;
	}

	public void setBand(String band) {
		Band = band;
	}

	public String getPercentile() {
		return Percentile;
	}

	public void setPercentile(String percentile) {
		Percentile = percentile;
	}

	public String getScoreReasons() {
		return scoreReasons;
	}

	public void setScoreReasons(String scoreReasons) {
		this.scoreReasons = scoreReasons;
	}

	public int getCreaditScoreConsumerNo() {
		return creaditScoreConsumerNo;
	}

	public void setCreaditScoreConsumerNo(int creaditScoreConsumerNo) {
		this.creaditScoreConsumerNo = creaditScoreConsumerNo;
	}

	public String getFraudPermission() {
		return FraudPermission;
	}

	public void setFraudPermission(String String) {
		FraudPermission = String;
	}

	public String getDateRequested() {
		return dateRequested;
	}

	public void setDateRequested(String dateRequested) {
		this.dateRequested = dateRequested;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getAuditLog() {
		return AuditLog;
	}

	public void setAuditLog(String auditLog) {
		AuditLog = auditLog;
	}

	public String getRating() {
		return Rating;
	}

	public void setRating(String rating) {
		Rating = rating;
	}

	public Map<String, String> getReasonCodeAndValueMap() {
		return reasonCodeAndValueMap;
	}

	public void setReasonCodeAndValueMap(Map<String, String> reasonCodeAndValueMap) {
		this.reasonCodeAndValueMap = reasonCodeAndValueMap;
	}

	public boolean isEmailPasswordiValid() {
		return isEmailPasswordValid;
	}

	public void setEmailPasswordiValid(boolean isEmailPasswordiValid) {
		this.isEmailPasswordValid = isEmailPasswordiValid;
	}

	public String getUserAuthenticationFailedMsg() {
		return userAuthenticationFailedMsg;
	}

	public void setUserAuthenticationFailedMsg(String userAuthenticationFailedMsg) {
		this.userAuthenticationFailedMsg = userAuthenticationFailedMsg;
	}

	public String getResponseResultErrorMsg() {
		return responseResultErrorMsg;
	}

	public void setResponseResultErrorMsg(String responseResultErrorMsg) {
		this.responseResultErrorMsg = responseResultErrorMsg;
	}

}
