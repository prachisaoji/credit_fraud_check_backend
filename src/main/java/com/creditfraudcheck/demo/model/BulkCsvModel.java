package com.creditfraudcheck.demo.model;

public class BulkCsvModel {

	private String surName;
	

	private String foreName1;
	private String idNumber;
	private String cellNumber;

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public String getForeName1() {
		return foreName1;
	}

	public void setForeName1(String foreName1) {
		this.foreName1 = foreName1;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getCellNumber() {
		return cellNumber;
	}

	public void setCellNumber(String cellNumber) {
		this.cellNumber = cellNumber;
	}
	public BulkCsvModel(String surName, String foreName1, String idNumber, String cellNumber) {
		super();
		this.surName = surName;
		this.foreName1 = foreName1;
		this.idNumber = idNumber;
		this.cellNumber = cellNumber;
	}

	public BulkCsvModel(String surName) {
		super();
		this.surName = surName;
	}

	public BulkCsvModel() {
		// TODO Auto-generated constructor stub
	}

}
