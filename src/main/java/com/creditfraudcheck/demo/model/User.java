package com.creditfraudcheck.demo.model;

public class User {
	
	private String id;
	private String  firstName;
	private String lastName;
	private String  officeContactNumber;
	private String  mobileNumber;
	private String emailAddress;
	private String companyName;
	private String  companyRegNumber;
	private String companyVatNumber;
	private String password;
	private String userName;
	private String userType;
	private String account_locked;
	private String cellNumber;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getofficeContactNumber() {
		return officeContactNumber;
	}
	public void setofficeContactNumber(String officeNumber) {
		this.officeContactNumber = officeNumber;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompanyRegNumber() {
		return companyRegNumber;
	}
	public void setCompanyRegNumber(String regNumber) {
		this.companyRegNumber = regNumber;
	}
	public String getCompanyVatNumber() {
		return companyVatNumber;
	}
	public void setCompanyVatNumber(String vatNumber) {
		this.companyVatNumber = vatNumber;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getAccount_locked() {
		return account_locked;
	}
	public void setAccount_locked(String account_locked) {
		this.account_locked = account_locked;
	}
	public String getCellNumber() {
		return cellNumber;
	}
	public void setCellNumber(String cellNumber) {
		this.cellNumber = cellNumber;
	}
	
	

}
