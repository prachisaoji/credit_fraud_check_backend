package com.creditfraudcheck.demo.model;

public class CreaditAndFraudModel {

	private String firstName;
	private String lastName;
	private String idNumber;
	private String startDate;
	private String endDate;
	private String userEmailAddress;
	private String creditScoreReportFileName;
	private String cellNumber;
	private String userPassword;
	
	//for user hsitory
	private String userHistoryData[];

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getUserEmailAddress() {
		return userEmailAddress;
	}

	public void setUserEmailAddress(String userEmailAddress) {
		this.userEmailAddress = userEmailAddress;
	}

	public String getCreditScoreReportFileName() {
		return creditScoreReportFileName;
	}

	public void setCreditScoreReportFileName(String creditScoreReportFileName) {
		this.creditScoreReportFileName = creditScoreReportFileName;
	}

	public String getCellNumber() {
		return cellNumber;
	}

	public void setCellNumber(String cellNumber) {
		this.cellNumber = cellNumber;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String[] getUserHistoryData() {
		return userHistoryData;
	}

	public void setUserHistoryData(String[] userHistoryData) {
		this.userHistoryData = userHistoryData;
	}

}
