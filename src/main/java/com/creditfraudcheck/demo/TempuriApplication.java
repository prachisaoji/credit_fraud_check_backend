package com.creditfraudcheck.demo;

import java.io.EOFException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.xml.parsers.ParserConfigurationException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.xml.sax.SAXException;

@SpringBootApplication
public class TempuriApplication {

	public static void main(String[] args) throws EOFException, ClassNotFoundException, IOException, ParserConfigurationException, SAXException, NoSuchAlgorithmException {
		
		SpringApplication.run(TempuriApplication.class, args);
		
		
	}

}
