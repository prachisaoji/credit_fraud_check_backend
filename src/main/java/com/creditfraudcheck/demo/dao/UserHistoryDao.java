package com.creditfraudcheck.demo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.creditfraudcheck.demo.model.User;
import com.creditfraudcheck.demo.model.UserHistoryModel;
import com.creditfraudcheck.demo.util.CreateExcel;

@Component
public class UserHistoryDao {

	@Autowired
	CreateExcel createExcel;
	@Autowired
	LoginDao loginDao;

	Logger logger = Logger.getLogger(UserHistoryDao.class.getName());

	DbConnection db = null;
	Connection con;
	PreparedStatement preparedStatement = null;
	Statement stmt = null;
	ResultSet rs = null;

	// insert into user_history table
	public void insertToUserHistoryTable(String apiServiceName, String userName, String searchFirstName,
			String searchLastName, String searchIdNumber, String searchCellNumber, String responseMessage,
			String scoreVlaue, String scoreReasons) throws SQLException {// emailAddress=userName

		logger.info("In  insertToUserHistoryTable() method  for first name " + searchFirstName + " , last name "
				+ searchLastName + " and, idNumber " + searchIdNumber + " , and search cell number " + searchCellNumber
				+ ", and  userName " + userName + " ,responseMessage " + responseMessage);
		User user = new User();
		user = loginDao.getUserdetailsFromDataBase(userName);
		String userId = user.getId();

		try {
			String sql = "insert into user_history (api_service,user_name,search_firstname,search_lastname,search_idnumber,search_cellnumber,response_status,creadit_score,score_reasons,userid) values(?,?,?,?,?,?,?,?,?,?)";

			db = new DbConnection();
			con = db.getConnection();
			preparedStatement = (PreparedStatement) con.prepareStatement(sql);
			preparedStatement.setString(1, apiServiceName);
			preparedStatement.setString(2, userName);
			preparedStatement.setString(3, searchFirstName);
			preparedStatement.setString(4, searchLastName);
			preparedStatement.setString(5, searchIdNumber);
			preparedStatement.setString(6, searchCellNumber);
			preparedStatement.setString(7, responseMessage);// responseMessage=response_status
			preparedStatement.setString(8, scoreVlaue);// scoreVlaue=creadit_score
			preparedStatement.setString(9, scoreReasons);// scoreVlaue=score_reasons
			preparedStatement.setString(10, userId);

			int i = preparedStatement.executeUpdate();

			if (i > 0) {
				logger.info("user history details added sucessfully in database ");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		} finally {
			con.close();
		}
	}

	// get details from user_history,user table
	public List<UserHistoryModel> getAllUserHistoryDeatilsList(String firstName, String lastName, String idNumber,
			String startDate, String endDate, String fileName, String userType, String userEmailAddress)
			throws SQLException {

		logger.info("In getAllUserHistoryDeatilsList() method    ");
		UserHistoryModel userHistoryModel = null;
		String sqlQuery = "";
		User userModel = new User();
		List<UserHistoryModel> userHistoryList = new ArrayList<UserHistoryModel>();
		try {
			if (userType.equals("admin")) {
				sqlQuery = "select * from user_history  where 1=1 ";
			} else {

				userModel = loginDao.getUserdetailsFromDataBase(userEmailAddress);
				String userId = userModel.getId();
				sqlQuery = "select * from user_history where  userid= '" + userId + "'";

			}
			// check null conditions appent where clause
			if (firstName.length() != 0) {
				sqlQuery += " AND";
				sqlQuery += " search_firstname ='" + firstName + "'";
			}

			if (lastName.length() != 0) {

				sqlQuery += " AND";
				sqlQuery += " search_lastname ='" + lastName + "'";

			}

			if (idNumber.length() != 0) {

				sqlQuery += " AND";
				sqlQuery += " search_idnumber='" + idNumber + "'";

			}
			// check for start date and end date
			if (startDate != null && endDate != null && startDate.length() != 0 && endDate.length() != 0) {

				sqlQuery += " AND";

				sqlQuery += " DATE(date) BETWEEN '" + startDate + "' And '" + endDate + "'";
			} else {
				// check only for start date
				if (startDate != null && startDate.length() != 0) {

					sqlQuery += " AND";

					sqlQuery += " DATE(date) = '" + startDate + "'";
				}

				// check for end date only
				if (endDate != null && endDate.length() != 0) {

					/*
					 * if (!sqlQuery.contains("AND")) { sqlQuery += " AND"; } else { sqlQuery +=
					 * " OR"; }
					 */
					sqlQuery += " AND";

					sqlQuery += " DATE(date)= '" + endDate + "'";
				}

			}

			sqlQuery += "ORDER BY date desc";
			// System.out.print(sqlQuery);

			db = new DbConnection();
			con = db.getConnection();
			stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = stmt.executeQuery(sqlQuery);

			if (rs.next()) {

				logger.info("Obtained Resultset object is not empty its contents are:");

				rs.beforeFirst();

				while (rs.next()) {
					userHistoryModel = new UserHistoryModel();
					userHistoryModel = extractUserHistorydData(rs);
					logger.info("getting details from user hsitory table");
					userHistoryList.add(userHistoryModel);

				}
				rs.beforeFirst();

				// calling exportToExcelFile method
				createExcel.exportToExcelFile(rs, fileName);
			} else {
				logger.info("Obtained ResultSet object is empty");

			}

		} catch (Exception e) {
			System.out.println(e);
			logger.info(" coming exception in getAllUserHistoryDeatilsList() method " + e);
		} finally {
			con.close();
		}
		return userHistoryList;

	}

	public UserHistoryModel extractUserHistorydData(ResultSet rs) {
		UserHistoryModel userHistoryModel = new UserHistoryModel();

		try {
			userHistoryModel.setApiService(rs.getString("api_service"));
			userHistoryModel.setId(rs.getString("id"));
			userHistoryModel.setDate(rs.getString("date"));
			String userName = rs.getString("user_name");
			userHistoryModel.setUserName(userName);
			userHistoryModel.setFirstName(rs.getString("search_firstname"));
			userHistoryModel.setLastName(rs.getString("search_lastname"));
			userHistoryModel.setIdNumber(rs.getString("search_idnumber"));
			userHistoryModel.setCellNumber(rs.getString("search_cellnumber"));
			userHistoryModel.setResponseStatus(rs.getString("response_status"));
		} catch (Exception e) {
			System.out.println(e);
			logger.info("Exception coming in extractUserHistorydData() method " + e);
		}
		return userHistoryModel;
	}

	// get list of registered userlist
	public List<User> getRegisteredUserList() throws SQLException {

		User user = null;

		logger.info("In getRegisteredUserList() method");
		List<User> userList = new ArrayList<User>();
		try {
			String sqlQuery = "select * from user WHERE user_type='user' ";
			db = new DbConnection();
			con = db.getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				user = new User();
				user.setUserName(rs.getString("first_name") + " " + rs.getString("last_name"));
				user.setEmailAddress(rs.getString("email_address"));
				user.setCompanyName(rs.getString("company_name"));
				user.setMobileNumber(rs.getString("mobile_number"));
				user.setofficeContactNumber(rs.getString("office_contact_number"));
				user.setPassword(rs.getString("password"));
				user.setAccount_locked(rs.getString("account_locked"));

				logger.info("getting details from user  table");
				userList.add(user);

			}

		} catch (Exception e) {
			System.out.println(e);
			logger.info(" coming exception in getRegisteredUserList() method " + e);
		} finally {
			con.close();
		}

		return userList;
	}

	public void clearUserHistoryData(String historyIdArray[]) throws SQLException {
		logger.info(" In clearUserHistoryData() method");
		try {
			for (int i = 0; i < historyIdArray.length; i++) {

				String historyId = historyIdArray[i].toString().replace(",", "") // remove the commas
						.replace("[", "") // remove the right bracket
						.replace("]", "") // remove the left bracket
						.trim(); // remove trailing spaces from partially initialized arrays;
				String sqlQuery = "DELETE from user_history WHERE id='" + historyId + "'";

				db = new DbConnection();
				con = db.getConnection();
				stmt = con.createStatement();
				int historyIdDeleted = stmt.executeUpdate(sqlQuery);
				
				if (historyIdDeleted > 0) {
					logger.info(" history id deleted from user_history table");
				}

			}
		}

		catch (Exception e) {
			System.out.println(e);
			logger.info(" coming exception in clearUserHistoryData() method " + e);
		} finally {
			con.close();
		}

	}

}
