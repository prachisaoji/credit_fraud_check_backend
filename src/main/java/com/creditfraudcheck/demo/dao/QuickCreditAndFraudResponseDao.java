package com.creditfraudcheck.demo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

@Component
public class QuickCreditAndFraudResponseDao {

	DbConnection db = null;
	Connection con;
	PreparedStatement preparedStatement = null;
	Statement stmt = null;
	ResultSet rs = null;

	Logger logger = Logger.getLogger(QuickCreditAndFraudResponseDao.class.getName());

	// insert QuickCreditAndFraud JSON response in quickcreditandfraudresponsetable
	// table
	public void insertQuickCreditAndFraudResponseToDb(String quickCreditAndFraudJsonResponse, String searchFirstName,
			String searchLastName, String searchIdNumber, String searchDateAndTime) throws SQLException {

		logger.info("In  insertQuickCreditAndFraudResponseToDb() method  for first name " + searchFirstName
				+ " , last name " + searchLastName + "  and idNumber " + searchIdNumber);

		try {
			String sql = "insert into quickcreditandfraudresponsetable (creditfraudresultjson,search_firstname,search_lastnmae,search_idnumber,date_time_stamp) values(?,?,?,?,?)";

			db = new DbConnection();
			con = db.getConnection();
			preparedStatement = (PreparedStatement) con.prepareStatement(sql);
			preparedStatement.setString(1, quickCreditAndFraudJsonResponse);
			preparedStatement.setString(2, searchFirstName);
			preparedStatement.setString(3, searchLastName);
			preparedStatement.setString(4, searchIdNumber);
			preparedStatement.setString(5, searchDateAndTime);

			int i = preparedStatement.executeUpdate();

			if (i > 0) {
				logger.info(
						"credit score JSON response details,firstName, lastName, nationalid added sucessfully in database");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			con.close();
		}
	}

	// get QuickCreditAndFraudJsonresult from quickcreditandfraudresponsetable
	public Map<String, String> getQuickCreditAndFraudJsonResultFromDb(String firstName, String lastName,
			String idNumber) throws SQLException {
		Map<String, String> quickCreditAndFraudJsonResult = null;

		logger.info("In  getQuickCreditAndFraudJsonResultFromDb() method for first name: " + firstName
				+ " and last name: " + lastName + " , nationalid: " + idNumber);

		DbConnection db = new DbConnection();
		Connection conn = db.getConnection();
		Statement stmt = null;
		try {

			String sql = "select  creditfraudresultjson  from quickcreditandfraudresponsetable WHERE  search_firstname='"
					+ firstName + "' AND search_lastnmae ='" + lastName + "' AND  search_idnumber ='" + idNumber
					+ "' order by id desc limit 1 ";
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			quickCreditAndFraudJsonResult = new HashMap<>();
			while (rs.next()) {
				String result = rs.getString("creditfraudresultjson");
				quickCreditAndFraudJsonResult.put("json result", result);

				logger.info("Getting the creditfraudresultjson from database successfully");

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("Exception in  getQuickCreditAndFraudJsonResultFromDb(): " + e);
		} finally {
			conn.close();
		}
		return quickCreditAndFraudJsonResult;
	}
//get data from scored_credit_reason table
	public Map<String, String> getReasonCodeAndValue(String[] reasonCodeArray) throws SQLException {
		Map<String, String> reasonCodeAndValueMap = new HashMap<>();
		logger.info("In getReasonCodeAndValue() method");
		DbConnection db = new DbConnection();
		Connection conn = db.getConnection();
		Statement stmt = null;
		try {
			for (int i = 0; i < reasonCodeArray.length; i++) {
				String reasonCode = reasonCodeArray[i].toString().replace(",", "") // remove the commas
						.replace("[", "") // remove the right bracket
						.replace("]", "") // remove the left bracket
						.trim(); // remove trailing spaces from partially initialized arrays;
				String sql = "SELECT * FROM scored_credit_reason where reason_code='" + reasonCode + "'";
				stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);

				while (rs.next()) {

					String reasonValue = rs.getString("reason_value");

					reasonCodeAndValueMap.put(reasonCode, reasonValue);
					logger.info("getting matched reson code value  from the database");

				}

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("Exception in  getQuickCreditAndFraudJsonResultFromDb(): " + e);
		} finally {
			conn.close();
		}

		Gson gson = new Gson();
		logger.info("reasonCodeAndValueMap response" + gson.toJson(reasonCodeAndValueMap));

		return reasonCodeAndValueMap;

	}
}
