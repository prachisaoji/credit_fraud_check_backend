package com.creditfraudcheck.demo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.creditfraudcheck.demo.model.User;

@Component
public class LoginDao {

	// logger
	final Logger logger = Logger.getLogger(LoginDao.class);
	DbConnection db = null;
	Connection con;
	Statement stmt = null;
	PreparedStatement preparedStatement = null;
	ResultSet rs = null;
	String mailId;
	String Password;

	// method to check email and password is available in user table or not
	public boolean checkEmailAddressPasswordExistInDb(String emailAddress, String password) throws SQLException {
		boolean login = false;
		logger.info("In  checkEmailAddressPasswordExistInDb() method");
		try {
			String sql = "Select email_address,password from user where email_address='" + emailAddress
					+ "' and password='" + password + "'";
			db = new DbConnection();
			con = db.getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				String dbEmailAddress = rs.getString("email_address");
				String dbPassword = rs.getString("password");

				if (dbEmailAddress.equals(emailAddress) && dbPassword.equals(password)) {
					System.out.println("login sucessfully");
					logger.info("login sucessfully with email address : " + emailAddress);
					login = true;
				} else {
					logger.info("Not login sucessfully with email address : " + emailAddress);

				}
			}

		}

		catch (Exception e) {
			logger.info("Exception coming in checkEmailAddressPasswordExistInDb() method  " + e);
		} finally {
			con.close();
		}
		return login;

	}

	// method to get user details from database
	public User getUserdetailsFromDataBase(String email_id) throws SQLException {
		User user = null;
		logger.info("In userdetails() method for email address : " + email_id);
		try {
			String sql = "select * from user u " + " where u.email_address='" + email_id + "'";
			db = new DbConnection();
			con = db.getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				user = new User();
				user.setId(rs.getString("id"));
				user.setFirstName(rs.getString("first_name"));
				user.setLastName(rs.getString("last_name"));
				user.setUserType(rs.getString("user_type"));
				user.setId(rs.getString("id"));
				user.setAccount_locked(rs.getString("account_locked"));
				user.setPassword(rs.getString("password"));
				logger.info("getting user details from user table from the database");

			}

		}

		catch (Exception e) {
			System.out.println(e);
			logger.info("Exception coming in getUserdetailsFromDataBase() method  " + e);

		} finally {
			con.close();
		}
		return user;

	}

	// method to check email available in user table or not
	public boolean checkEmailAddressExistInDb(String emailAddress) throws SQLException {
		boolean login = false;
		logger.info("In  checkEmailAddressExistInDb() method");
		try {
			String sql = "Select email_address,password from user where email_address='" + emailAddress + "'";
			db = new DbConnection();
			con = db.getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				String dbEmailAddress = rs.getString("email_address");

				if (dbEmailAddress.equals(emailAddress)) {
					logger.info(" email address exist in database  : " + emailAddress);
					login = true;
				} else {
					logger.info("email address not avialable in database: " + emailAddress);

				}
			}

		}

		catch (Exception e) {
			logger.info("Exception coming in checkEmailAddressPasswordExistInDb() method  " + e);
		} finally {
			con.close();
		}
		return login;

	}

	// Updating locked and unlocked checked in user table
	public void toUpdateLockedUnlockedCheckedToDb(String registeredEmailAddress, String lockedUnlockedValue)
			throws SQLException {
		logger.info("In toUpdateLockedUnlockedCheckedToDb() method for email address : " + registeredEmailAddress);
		try {

			String sql = "";

			sql = "UPDATE  user SET account_locked='" + lockedUnlockedValue + "' where email_address='"
					+ registeredEmailAddress + "'";

			db = new DbConnection();
			con = db.getConnection();
			stmt = con.createStatement();
			int accountLockedUpdated = stmt.executeUpdate(sql);
			if (accountLockedUpdated > 0) {
				logger.info("An account_locked :  " + lockedUnlockedValue
						+ "  field was updated successfully in the database ");
			}

		}

		catch (Exception e) {
			System.out.println(e);
			logger.info("Exception coming in toUpdateLockedUnlockedCheckedToDb() method  " + e);
		} finally {
			con.close();
		}

	}

}
