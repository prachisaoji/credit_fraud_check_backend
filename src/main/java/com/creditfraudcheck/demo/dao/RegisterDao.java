package com.creditfraudcheck.demo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.creditfraudcheck.demo.util.MailUtility;

@Component
public class RegisterDao {
	// logger
	final Logger logger = Logger.getLogger(RegisterDao.class);
	@Autowired
	MailUtility mailUtility;

	DbConnection db = null;
	Connection con;
	PreparedStatement preparedStatement = null;
	Statement stmt = null;
	ResultSet rs = null;

	// insert into user table
	public void insertToUserTable(String firstName, String lastName, String officeConatctNumber, String mobileNumber,
			String emailAddress, String companyName, String companyRegNumber, String companyVatNumber)
			throws SQLException {

		logger.info("In  insertToUserTable() method  ");
		String password = randomPasswordGenrator();

		try {
			String sql = "insert into user (id,first_name,last_name,office_contact_number,mobile_number,email_address,company_name,company_reg_number,company_vat_number,password) values(?,?,?,?,?,?,?,?,?,?)";

			db = new DbConnection();
			con = db.getConnection();
			preparedStatement = (PreparedStatement) con.prepareStatement(sql);
			String uniqueID = UUID.randomUUID().toString();
			preparedStatement.setString(1, uniqueID);
			preparedStatement.setString(2, firstName);
			preparedStatement.setString(3, lastName);
			preparedStatement.setString(4, officeConatctNumber);
			preparedStatement.setString(5, mobileNumber);
			preparedStatement.setString(6, emailAddress);
			preparedStatement.setString(7, companyName);
			preparedStatement.setString(8, companyRegNumber);
			preparedStatement.setString(9, companyVatNumber);
			preparedStatement.setString(10, password);

			int i = preparedStatement.executeUpdate();

			if (i > 0) {
				logger.info("user details added sucessfully in database ");
				mailUtility.sendEmailAddressAndPwdToAdmin(emailAddress, password);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			con.close();
		}
	}

	// for to check email is already exist
	public boolean checkEmailIsAlreadyExist(String emailAddress) throws SQLException {
		boolean userAlreadyExist = false;
		logger.info("In checkEmailIsAlreadyExist() method for emailAddress: " + emailAddress);
		try {
			String sql = "Select  email_address from user where email_address='" + emailAddress + "'";
			db = new DbConnection();
			con = db.getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				emailAddress = rs.getString("email_address");
				logger.info("user is already reagistered ");
				userAlreadyExist = true;
			}
		}

		catch (Exception e) {
			System.out.println(e);
		} finally {
			con.close();
		}
		return userAlreadyExist;

	}
	
	

	public String randomPasswordGenrator() {
		String password = null;
		int length = 10;
		logger.info("In randomPasswordGenrator() method ");

		try {
			String capitalLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			String smallChars = "abcdefghijklmnopqrstuvwxyz";
			String numbers = "0123456789";
			String symbol = "!@#$%^&_=+-";
			password = selectAChar(capitalLetters) + selectAChar(smallChars) + selectAChar(smallChars)
					+ selectAChar(smallChars) + selectAChar(smallChars) + selectAChar(numbers) + selectAChar(numbers)
					+ selectAChar(numbers) + selectAChar(symbol) + selectAChar(symbol);

			logger.info("Random generated password is : " + password);
		} catch (Exception e) {
			System.out.println(e);
		}
		return password;
	}

	public static String selectAChar(String s) {
		Random random = new Random();
		int index = random.nextInt(s.length());
		return "" + s.charAt(index);
	}
}
