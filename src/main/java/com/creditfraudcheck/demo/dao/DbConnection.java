package com.creditfraudcheck.demo.dao;


import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DbConnection {

	Properties props = new Properties();
	FileInputStream fis = null;
	Connection con = null;

	public java.sql.Connection getConnection() {
		try {
			InputStream inputStream = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("application.properties");
			props.load(inputStream);
			Class.forName(props.getProperty("DB_DRIVER_CLASS"));
			// create the connection now
			con = DriverManager.getConnection(props.getProperty("DB_URL"), props.getProperty("DB_USERNAME"),
					props.getProperty("DB_PASSWORD"));
			System.out.println("sql connection established");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Connection Failed! : " + e);
		}
		return con;
	}

	public void closeConnection(Connection con) throws SQLException {
		con.close();
	}
}
