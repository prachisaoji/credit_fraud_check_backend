package com.creditfraudcheck.demo.appConfig;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.creditfraudcheck.demo.model.BulkCsvModel;
import com.creditfraudcheck.demo.util.ControllerMethodSupportUtil;
import com.creditfraudcheck.demo.util.DoQuickCreditAndFraudApiRequest;

@Service
public class AsynchronousClass {
	Logger logger = Logger.getLogger(AsynchronousClass.class.getName());
	@Autowired
	DoQuickCreditAndFraudApiRequest doQuickCreditAndFraudApiRequest;
	@Autowired
	ControllerMethodSupportUtil controllerMethodSupportUtil;

	/*
	 * @Async("asyncExecutor") public void
	 * asynchronousCreditFraudAndSmartleadsApiCall(List<BulkCsvModel>
	 * bulkCsvModelList, String fileNameWithGenratedString, String emailAddress) {
	 * 
	 * logger.
	 * info("In asynchronousCreditFraudAndSmartleadsApiCall method  for fileNameWithGenratedString : "
	 * + fileNameWithGenratedString + "  and , emailAddress " + emailAddress);
	 * 
	 * boolean isBulkCsvUploadFlag = true;
	 * 
	 * try {
	 * 
	 * for (BulkCsvModel bulkCsvModel : bulkCsvModelList) {
	 * 
	 * logger.
	 * info(" In asynchronousCreditFraudAndSmartleadsApiCall method  for loop ");
	 * 
	 * long threadId = Thread.currentThread().getId(); logger.info("**Thread # " +
	 * threadId + " is doing this task"); try { if
	 * (bulkCsvModel.getCellNumber().trim().length() != 0 &&
	 * bulkCsvModel.getCellNumber().trim() != "") { try {
	 * logger.info(bulkCsvModel.getCellNumber().trim().length());
	 * 
	 * logger.info(
	 * "getting cellnumber  is not empty from csv file so calling the trace user API(SmartLeads)"
	 * );
	 * 
	 * controllerMethodSupportUtil.traceUserApiCalledSupportingMethod(bulkCsvModel,
	 * isBulkCsvUploadFlag, fileNameWithGenratedString, emailAddress);
	 * Thread.sleep(1000L); } catch (Exception e) { e.printStackTrace(); }
	 * 
	 * } else { try { logger.info(
	 * "getting cellnumber  is empty from csv file so calling the credit fraud check API  "
	 * );
	 * 
	 * controllerMethodSupportUtil.creditFraudCheckApiSupportMethod(bulkCsvModel,
	 * isBulkCsvUploadFlag, fileNameWithGenratedString, emailAddress);
	 * Thread.sleep(1000L); } catch (Exception e) { e.printStackTrace(); }
	 * 
	 * } // Thread.sleep(10000); } catch (Exception e) { e.printStackTrace(); }
	 * 
	 * } logger.info("***  asynchronous run method end ****");
	 * 
	 * } catch (Exception e) { logger.
	 * info(" exception occur in asynchronousCreditFraudAndSmartleadsApiCall() method"
	 * + e); e.printStackTrace(); } }
	 */

	public void asynchronousCreditFraudAndSmartleadsApiCall(List<BulkCsvModel> bulkCsvModelList,
			String fileNameWithGenratedString, String emailAddress) {

		logger.info("In asynchronousCreditFraudAndSmartleadsApiCall method  for fileNameWithGenratedString : "
				+ fileNameWithGenratedString + "  and , emailAddress " + emailAddress);

		boolean isBulkCsvUploadFlag = true;
		ExecutorService executorService = Executors.newSingleThreadExecutor();

		try {

			executorService.execute(new Runnable() {

				public void run() {

					logger.info("Asynchronous run method start");
					int count = 0;

					for (BulkCsvModel bulkCsvModel : bulkCsvModelList) {

						count++;
						
						logger.info(" In asynchronousCreditFraudAndSmartleadsApiCall method  for loop ");
						
						logger.info(" Processing Row " + count + " of " + bulkCsvModelList.size());

						

						try {
							if (bulkCsvModel.getCellNumber().trim().length() != 0
									&& bulkCsvModel.getCellNumber().trim() != "") {
								try {
									logger.info(bulkCsvModel.getCellNumber().trim().length());

									logger.info(
											"getting cellnumber  is not empty from csv file so calling the trace user API(SmartLeads)");

									controllerMethodSupportUtil.traceUserApiCalledSupportingMethod(bulkCsvModel,
											isBulkCsvUploadFlag, fileNameWithGenratedString, emailAddress);

								} catch (Exception e) {
									e.printStackTrace();
									logger.error(e.getMessage());
								}

							} else {
								try {
									logger.info(
											"getting cellnumber  is empty from csv file so calling the credit fraud check API  ");

									controllerMethodSupportUtil.creditFraudCheckApiSupportMethod(bulkCsvModel,
											isBulkCsvUploadFlag, fileNameWithGenratedString, emailAddress);

								} catch (Exception e) {
									e.printStackTrace();
									logger.error(e.getMessage());
								}

							}
							// Thread.sleep(10000);
						} catch (Exception e) {
							e.printStackTrace();
							logger.error("Error encountered in processing of row: " + count + " : " + e.getMessage());
						}

					}
					logger.info("***  asynchronous run method end ****");
				}
			});

			executorService.shutdown();

		} catch (Throwable e) {
			logger.error(" exception occur in asynchronousCreditFraudAndSmartleadsApiCall() method" + e);
			e.printStackTrace();
		}
	}

}
