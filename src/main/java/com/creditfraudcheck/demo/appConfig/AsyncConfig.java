package com.creditfraudcheck.demo.appConfig;

import java.util.concurrent.Executor;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
public class AsyncConfig {
	Logger logger = Logger.getLogger(AsyncConfig.class.getName());

	//@Bean
	 @Bean(name = "asyncExecutor")
	public Executor asyncExecutor() {
		logger.info("asyncExecutor () method ");
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(10);
		executor.setMaxPoolSize(10);
		executor.setQueueCapacity(1000);
		executor.setThreadNamePrefix("Async Process-");
		executor.initialize();
		return executor;
	}

	// Define a Spring bean of type TestAsyncClazz
	@Bean
	public AsynchronousClass testAsyncClass() {
		logger.info("testAsyncClass () method ");

		return new AsynchronousClass();
	}

}
