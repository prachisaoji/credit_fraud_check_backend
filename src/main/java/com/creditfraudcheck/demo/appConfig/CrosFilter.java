package com.creditfraudcheck.demo.appConfig;
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public   class CrosFilter  implements Filter {

	  public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
	        // Lets make sure that we are working with HTTP (that is, against HttpServletRequest and HttpServletResponse objects)
	        if (req instanceof HttpServletRequest && res instanceof HttpServletResponse) {
	            HttpServletRequest request = (HttpServletRequest) req;
	            HttpServletResponse response = (HttpServletResponse) res;
	            // Access-Control-Allow-Origin
	            String origin = request.getHeader("Origin");
	            //response.setHeader("Access-Control-Allow-Origin", allowedOrigins.contains(origin) ? origin : "");
	            response.setHeader("Access-Control-Allow-Origin", "*");
	            response.setHeader("Vary", "Origin");
	            // Access-Control-Max-Age
	            response.setHeader("Access-Control-Max-Age", "86400");
	            // Access-Control-Allow-Credentials
	            response.setHeader("Access-Control-Allow-Credentials", "true");
	            // Access-Control-Allow-Methods
	            response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
	            // Access-Control-Allow-Headers
	            response.setHeader("Access-Control-Allow-Headers",
	                "Origin, X-Requested-With,Authorization,authorization, Content-Type, Accept, ");
	            //response.setHeader("Access-Control-Allow-Headers","*");
	        }
	        chain.doFilter(req, res);
	    }


}
