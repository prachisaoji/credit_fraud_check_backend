package com.creditfraudcheck.demo.controller;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.creditfraudcheck.demo.appConfig.AsynchronousClass;
import com.creditfraudcheck.demo.dao.LoginDao;
import com.creditfraudcheck.demo.dao.QuickCreditAndFraudResponseDao;
import com.creditfraudcheck.demo.dao.RegisterDao;
import com.creditfraudcheck.demo.dao.UserHistoryDao;
import com.creditfraudcheck.demo.model.BulkCsvModel;
import com.creditfraudcheck.demo.model.CreaditAndFraudModel;

import com.creditfraudcheck.demo.model.QuickCreditAndFraudResponseResult;
import com.creditfraudcheck.demo.model.User;
import com.creditfraudcheck.demo.model.UserHistoryModel;

import com.creditfraudcheck.demo.util.ControllerMethodSupportUtil;
import com.creditfraudcheck.demo.util.DoQuickCreditAndFraudApiRequest;
import com.creditfraudcheck.demo.util.ReadCsvFile;
import com.creditfraudcheck.demo.util.TraceUserSmartleadsv2ApiRequest;
import com.creditfraudcheck.demo.util.Util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api")
public class TempUriController {

	Logger logger = Logger.getLogger(TempUriController.class.getName());
	@Autowired
	DoQuickCreditAndFraudApiRequest doQuickCreditAndFraudApiRequest;
	@Autowired
	Util util;

	@Autowired
	RegisterDao registerDao;
	@Autowired
	LoginDao loginDao;
	@Autowired
	UserHistoryDao userHistoryDao;

	@Autowired
	TraceUserSmartleadsv2ApiRequest traceUserSmartleadsv2ApiRequest;

	@Autowired
	QuickCreditAndFraudResponseDao quickCreditAndFraudResponseDao;

	@Autowired
	AsynchronousClass asynchronousClass;

	@Autowired
	ReadCsvFile readCsvFile;

	@Autowired
	ControllerMethodSupportUtil controllerMethodSupportUtil;

	// for QuickCreditAndFraudResponseRequest

	@RequestMapping(value = "/creditandfraudinfo", method = RequestMethod.POST)
	public QuickCreditAndFraudResponseResult postCreaditAndFraudInfo(
			@RequestBody CreaditAndFraudModel creaditAndFraudModel, RedirectAttributes redirectAttrs)
			throws EOFException, ClassNotFoundException, IOException, SQLException {

		String[] responseXmlAndResponseMessage = null;
		boolean isBulkCsvUploadFlag = false;

		QuickCreditAndFraudResponseResult quickCreditAndFraudResponseResult = new QuickCreditAndFraudResponseResult();

		logger.info(" In  postCreaditAndFraudInfo() method  for first name " + creaditAndFraudModel.getFirstName()
				+ " , last name " + creaditAndFraudModel.getLastName() + "  and idNumber "
				+ creaditAndFraudModel.getIdNumber() + ", email adddress " + creaditAndFraudModel.getUserEmailAddress()
				+ ", cell number " + creaditAndFraudModel.getCellNumber() + ", and  user password "
				+ creaditAndFraudModel.getUserPassword());

		// checking email address and password already exist or not in database
		if (loginDao.checkEmailAddressPasswordExistInDb(creaditAndFraudModel.getUserEmailAddress(),
				creaditAndFraudModel.getUserPassword())) {
			logger.info("email address and password is exist in database");

			try {
				responseXmlAndResponseMessage = doQuickCreditAndFraudApiRequest.doQuickCreditAndFraudRequest(
						creaditAndFraudModel.getFirstName().trim(), creaditAndFraudModel.getLastName().trim(),
						creaditAndFraudModel.getIdNumber().trim(), isBulkCsvUploadFlag);
			} catch (ClassNotFoundException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String doQuickCreditAndFraudResponseXMLResult = responseXmlAndResponseMessage[0];
			String responseMessage = responseXmlAndResponseMessage[1];
			if (!responseMessage.equals("Internal Server Error")) {
				if (doQuickCreditAndFraudResponseXMLResult.trim().length() > 0
						&& doQuickCreditAndFraudResponseXMLResult != null && responseMessage.trim().length() > 0
						&& responseMessage != null) {
					try {

						quickCreditAndFraudResponseResult = doQuickCreditAndFraudApiRequest
								.getQuickCreditAndFraudResponseResult(doQuickCreditAndFraudResponseXMLResult,
										creaditAndFraudModel.getFirstName(), creaditAndFraudModel.getLastName(),
										creaditAndFraudModel.getIdNumber(), creaditAndFraudModel.getCellNumber(),
										creaditAndFraudModel.getUserEmailAddress(), responseMessage,
										isBulkCsvUploadFlag);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
				if (quickCreditAndFraudResponseResult.getIdentityNo1() == null) {
					quickCreditAndFraudResponseResult.setResponseResultErrorMsg("Data not found for first name "
							+ creaditAndFraudModel.getFirstName() + ", last name " + creaditAndFraudModel.getLastName()
							+ " and  Identity number " + creaditAndFraudModel.getIdNumber() + " .");

				}
			}
			quickCreditAndFraudResponseResult.setEmailPasswordiValid(true);
			quickCreditAndFraudResponseResult.setUserAuthenticationFailedMsg("User authentication sucessfully");
			return quickCreditAndFraudResponseResult;
		} else {
			logger.info("email address and password is not  exist in database");
			quickCreditAndFraudResponseResult.setEmailPasswordiValid(false);
			quickCreditAndFraudResponseResult.setUserAuthenticationFailedMsg("User authentication failed");
			return quickCreditAndFraudResponseResult;
		}

	}

	// get trace user and credit fraud info
	@RequestMapping(value = "/getTraceUserAndCreditAndFraudInfo", method = RequestMethod.POST)
	public QuickCreditAndFraudResponseResult postTraceUserCreaditAndFraudInfo(
			@RequestBody CreaditAndFraudModel creaditAndFraudModel, RedirectAttributes redirectAttrs)
			throws SQLException {
		String[] responseXmlAndResponseMessage = null;
		String[] traceUserApiParseValues;
		String traceUserSmartleadsv2ApiJsonResult = "";

		String lastName = "";
		String firstName = "";
		String idNumber = "";
		boolean isBulkCsvUploadFlag = false;

		logger.info(" In  postTraceUserCreaditAndFraudInfo() method  for last name "
				+ creaditAndFraudModel.getLastName() + " and cellNumber " + creaditAndFraudModel.getCellNumber()
				+ ", email adddress " + creaditAndFraudModel.getUserEmailAddress() + ", and  id number "
				+ creaditAndFraudModel.getIdNumber() + ", and  user password "
				+ creaditAndFraudModel.getUserPassword());

		QuickCreditAndFraudResponseResult quickCreditAndFraudResponseResult = new QuickCreditAndFraudResponseResult();

		// checking email address and password already exist or not in database
		if (loginDao.checkEmailAddressPasswordExistInDb(creaditAndFraudModel.getUserEmailAddress(),
				creaditAndFraudModel.getUserPassword())) {
			{
				logger.info("email address and password is is exist in database ");
				try {
					traceUserSmartleadsv2ApiJsonResult = traceUserSmartleadsv2ApiRequest
							.traceUserSmartleadsv2ApiRequest(creaditAndFraudModel.getLastName(),
									creaditAndFraudModel.getCellNumber());
				} catch (ClassNotFoundException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// if trace user API result get is null
				if (traceUserSmartleadsv2ApiJsonResult.trim().length() > 0
						&& traceUserSmartleadsv2ApiJsonResult != null) {
					traceUserApiParseValues = traceUserSmartleadsv2ApiRequest
							.parsTraceUserSmartleadsv2ApiResponse(traceUserSmartleadsv2ApiJsonResult);
					lastName = traceUserApiParseValues[0];
					firstName = traceUserApiParseValues[1];
					idNumber = traceUserApiParseValues[2];

					try {
						responseXmlAndResponseMessage = doQuickCreditAndFraudApiRequest.doQuickCreditAndFraudRequest(
								firstName.trim(), lastName.trim(), idNumber.trim(), isBulkCsvUploadFlag);
					} catch (ClassNotFoundException | IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					String doQuickCreditAndFraudResponseXMLResult = responseXmlAndResponseMessage[0];
					String responseMessage = responseXmlAndResponseMessage[1];

					if (doQuickCreditAndFraudResponseXMLResult.trim().length() > 0
							&& doQuickCreditAndFraudResponseXMLResult != null && responseMessage.trim().length() > 0
							&& responseMessage != null) {
						try {

							quickCreditAndFraudResponseResult = doQuickCreditAndFraudApiRequest
									.getQuickCreditAndFraudResponseResult(doQuickCreditAndFraudResponseXMLResult,
											firstName.trim(), lastName.trim(), idNumber.trim(),
											creaditAndFraudModel.getCellNumber(),
											creaditAndFraudModel.getUserEmailAddress(), responseMessage,
											isBulkCsvUploadFlag);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
				} else {

					// else traceUserSmartleadsv2ApiJsonResult is null so directly inserting in the
					// database

					userHistoryDao.insertToUserHistoryTable("doQuickCreditAndFraud",
							creaditAndFraudModel.getUserEmailAddress(), creaditAndFraudModel.getFirstName(),
							creaditAndFraudModel.getLastName(), creaditAndFraudModel.getIdNumber(),
							creaditAndFraudModel.getCellNumber(), "data not found", "", "");

					quickCreditAndFraudResponseResult.setResponseResultErrorMsg(
							"The id, name/surname combination does not match our home affairs verified data");
				}

			}
			quickCreditAndFraudResponseResult.setEmailPasswordiValid(true);
			quickCreditAndFraudResponseResult.setUserAuthenticationFailedMsg("User authentication sucessfully");

			return quickCreditAndFraudResponseResult;
		} else {

			logger.info("email address and password is not exist in database ");
			quickCreditAndFraudResponseResult.setEmailPasswordiValid(false);
			quickCreditAndFraudResponseResult.setUserAuthenticationFailedMsg("User authentication failed");

			return quickCreditAndFraudResponseResult;

		}

	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String registerUser(@RequestBody User user, RedirectAttributes redirectAttrs) throws Exception {
		String companyRegNumber = "";
		String companyVatNumber = "";
		String registerMsg = "";

		if (user.getCompanyRegNumber() != null) {
			companyRegNumber = user.getCompanyRegNumber();
		}
		if (user.getCompanyVatNumber() != null) {
			companyVatNumber = user.getCompanyVatNumber();
		}
		String emailAddress = user.getEmailAddress();
		// condition to check user is already registered by email address
		if (registerDao.checkEmailIsAlreadyExist(emailAddress)) {
			registerMsg = "You are already registered please signin first!";
			logger.info("user already registered");
			return registerMsg;

		} else {
			registerDao.insertToUserTable(user.getFirstName(), user.getLastName(), user.getofficeContactNumber(),
					user.getMobileNumber(), user.getEmailAddress(), user.getCompanyName(), companyRegNumber,
					companyVatNumber);

			registerMsg = "You are registered successfully, you will be contacted shortly.";
			logger.info("user already not registered");
			return registerMsg;
		}
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public Map login(@RequestBody User user, RedirectAttributes redirectAttrs) throws Exception {

		HashMap<String, Object> map = new HashMap<>();
		User userModel = new User();
		userModel = loginDao.getUserdetailsFromDataBase(user.getEmailAddress());

		if (userModel != null) {// if check email address already exist in database

			String accountLockedUnlockedValue = userModel.getAccount_locked();
			String password = userModel.getPassword();
			// checking the user account is already locked or not
			if (!accountLockedUnlockedValue.equals("locked")) {

				// checking email address and password is exist in database or not
				if (loginDao.checkEmailAddressPasswordExistInDb(user.getEmailAddress(), user.getPassword())) {
					// successMessage="correct details";
					logger.info("email address and password is valid ");
					// return isInvalid;
					String userType = userModel.getUserType();
					map.put("isEmailPwdInvalid", true);
					map.put("userEmailAddress", user.getEmailAddress());
					map.put("userType", userType);
					map.put("password", password);

					return map;
				}

				else {

					logger.info("email address and password is invalid ");
					// return isInvalid;
					map.put("isEmailPwdInvalid", false);
					return map;
				}
			}

			{// else when user account is locked

				logger.info("user account already has been locked");
				map.put("isAccountAlreadyLocked", false);
				return map;
			}

		} else {// else user is not registered

			logger.info("user is not registered");
			map.put("isUserNotRegistered", false);
			return map;
		}

	}

	@GetMapping("/saveCreditandfraudResponse")
	public Map<String, Object> saveCreditandfraudResponse(@RequestParam("fullname") String fullname,
			@RequestParam("nationalid") String nationalid, @RequestParam("email") String emailAddress)
			throws SQLException {

		Map<String, Object> mapObject = new HashMap<String, Object>();
		String firstName = "";
		String lastName = "";

		QuickCreditAndFraudResponseResult quickCreditAndFraudResponseResult = new QuickCreditAndFraudResponseResult();

		// if space is coming in the full name
		if (fullname.split("\\w+").length > 1) {
			int firstSpace = fullname.indexOf(" "); // detect the first space character
			firstName = fullname.substring(0, firstSpace); // get everything upto the first space character
			lastName = fullname.substring(firstSpace).trim();
			// get everything after the first space, trimming the
		}

		logger.info("In saveCreditandfraudResponse() method for fullname = " + fullname + " and nationalId = "
				+ nationalid + ", and  emailAddress" + emailAddress);
		// asynchronousClass.testAsyncMethod(firstName.trim(), lastName.trim(),
		// nationalid.trim(), emailAddress.trim());

		String message = "success";
		mapObject.put("message", message);
		return mapObject;

		// return JSONObject.quote("xhhhs");
	}

	@GetMapping("/getCreditandfraudResponse")
	public Map<String, String> getCreditandfraudResponse(@RequestParam("fullname") String fullname,
			@RequestParam("nationalid") String nationalid) throws SQLException {
		Map<String, String> quickCreditAndFraudJsonResult = new HashMap<String, String>();
		int firstSpace = fullname.indexOf(" "); // detect the first space character
		String firstName = fullname.substring(0, firstSpace); // get everything upto the first space character
		String lastName = fullname.substring(firstSpace).trim(); // get everything after the first space, trimming the
		logger.info("IN getCreditandfraudResponse() for first name: " + firstName + " and last name: " + lastName
				+ " , nationalid: " + nationalid); // spaces off
		quickCreditAndFraudJsonResult = quickCreditAndFraudResponseDao
				.getQuickCreditAndFraudJsonResultFromDb(firstName.trim(), lastName.trim(), nationalid.trim());// nationalid=idNumber

		logger.info("CreditandfraudJsonResponse: " + new Gson().toJson(quickCreditAndFraudJsonResult));

		logger.info("end getCreditandfraudResponse() method");
		return quickCreditAndFraudJsonResult;

	}

	
	@RequestMapping(value = "/etesiRequest", method = RequestMethod.POST)
	public ResponseEntity<Object> getCreditFraudResponse(@RequestHeader(value = "Username") String username,
			@RequestHeader(value = "Password") String password, @RequestBody CreaditAndFraudModel creaditAndFraudModel)
			throws SQLException {
		QuickCreditAndFraudResponseResult quickCreditAndFraudResponseResult = new QuickCreditAndFraudResponseResult();
		String doQuickCreditAndFraudResponseXMLResult = "";

		String responseXmlAndResponseMessage[];
		String responseMessage = "";
		boolean isBulkCsvUploadFlag = false;

		Map<String, Object> convertJsonToMapObject = new HashMap<String, Object>();

		// check password and user name exist or not in the database
		if (loginDao.checkEmailAddressPasswordExistInDb(username, password)) // username=emailaddress
		{

			logger.info("email address and password is valid ");

			try {
				responseXmlAndResponseMessage = doQuickCreditAndFraudApiRequest.doQuickCreditAndFraudRequest(
						creaditAndFraudModel.getFirstName().trim(), creaditAndFraudModel.getLastName().trim(),
						creaditAndFraudModel.getIdNumber().trim(), isBulkCsvUploadFlag);
				doQuickCreditAndFraudResponseXMLResult = responseXmlAndResponseMessage[0];
				responseMessage = responseXmlAndResponseMessage[1]; // username=emailaddress
			} catch (ClassNotFoundException | IOException e) {// doQuickCreditAndFraudResponseResultString=XML result of
																// API
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.info("ClassNotFoundException or  IOException " + e);
			}
			// if doQuickCreditAndFraudResponseXMLResult and responseMessage empty
			if (doQuickCreditAndFraudResponseXMLResult.trim().length() > 0
					&& doQuickCreditAndFraudResponseXMLResult != null && responseMessage.trim().length() > 0
					&& responseMessage != null) {
				try {
					convertJsonToMapObject = doQuickCreditAndFraudApiRequest.parseDoQuickCreditAndFraudResultXmmldata(
							doQuickCreditAndFraudResponseXMLResult, creaditAndFraudModel.getFirstName(),
							creaditAndFraudModel.getLastName(), creaditAndFraudModel.getIdNumber(),
							creaditAndFraudModel.getUserEmailAddress(), responseMessage);

				} catch (JsonProcessingException | JSONException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					logger.info("JsonProcessingException or  JSONException  or SQLException " + e);

				}
				// if convertJsonToMapObject is empty
				if (!convertJsonToMapObject.isEmpty() && convertJsonToMapObject != null) {

					logger.info("getCreditinfo() end");
					return new ResponseEntity<Object>(convertJsonToMapObject, HttpStatus.OK);
				} else {

					logger.info("getCreditinfo() end");
					return new ResponseEntity<Object>("Data not found", HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<Object>("Something wrong, please try again", HttpStatus.NOT_FOUND);
			}
		}

		else {
			logger.info("email address and password is invalid ");
			// return new ResponseEntity(HttpStatus.NOT_FOUND);
			return new ResponseEntity<Object>("Invalid username or password", HttpStatus.NOT_FOUND);

		}

	}

	@RequestMapping(value = "/getUserHistory/{userType}", method = RequestMethod.POST)
	public Map<String, Object> getUserHistoryDetails(@RequestBody CreaditAndFraudModel creaditAndFraudModel,
			@PathVariable("userType") String userType) throws SQLException {

		Map<String, Object> mapObject = new HashMap<>();
		logger.info("In getUserHistoryDetails() method  for first name" + creaditAndFraudModel.getFirstName()
				+ " and, last name " + creaditAndFraudModel.getLastName() + ", start date "
				+ creaditAndFraudModel.getStartDate() + " end date " + creaditAndFraudModel.getEndDate()
				+ " ,nationalIdNumber  " + creaditAndFraudModel.getIdNumber() + ", user type " + userType
				+ " user email address " + creaditAndFraudModel.getUserEmailAddress());
		// Generate randomFileName
		String generatedString = RandomStringUtils.randomAlphabetic(5);
		String fileNameWithGenratedString = "creaditscore-" + generatedString + ".xlsx";

		logger.info("Genrated file Name is: " + fileNameWithGenratedString);
		List<UserHistoryModel> userHistoryModelList = userHistoryDao.getAllUserHistoryDeatilsList(
				creaditAndFraudModel.getFirstName(), creaditAndFraudModel.getLastName(),
				creaditAndFraudModel.getIdNumber(), creaditAndFraudModel.getStartDate(),
				creaditAndFraudModel.getEndDate(), fileNameWithGenratedString, userType,
				creaditAndFraudModel.getUserEmailAddress());
		mapObject.put("genratedReportFileName", fileNameWithGenratedString);
		mapObject.put("listOfSearchUserHistory", userHistoryModelList);

		return mapObject;
	}

	// method to download excel file
	@GetMapping("download/{fileName:.+}")
	public void downloadReport(HttpServletResponse response, @PathVariable("fileName") String fileName)
			throws IOException, ClassNotFoundException {

		Properties prop = util.readPropertiesFile("application.properties");
		String excelFilePath = prop.getProperty("excelFilePath");

		logger.info("In downloadReport() method");

		String excelFileName = excelFilePath + fileName;
		try {
			File file = new File(excelFileName);

			if (file.exists() && !file.isDirectory()) {

				Files.copy(file.toPath(), response.getOutputStream());
				String mimeType = URLConnection.guessContentTypeFromName(file.getName());
				String contentDisposition = String.format("attachment; filename=%s", file.getName());
				int fileSize = Long.valueOf(file.length()).intValue();

				response.setContentType(mimeType);
				response.setHeader("Content-Disposition", contentDisposition);
				response.setContentLength(fileSize);
				logger.info("Report is downloded successfully with excel file name :" + excelFileName);
			} else {
				logger.info("file is not present in the file path");
			}

		} catch (FileNotFoundException fe) {
			fe.printStackTrace();
		} catch (IOException ie) {
			ie.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// to get user details
	@GetMapping("/registeredUserList")
	public List<User> getDetailsOfRegisteredUser() throws SQLException {
		List<User> registeredUserListDetails = userHistoryDao.getRegisteredUserList();
		logger.info("In getDetailsOfRegisteredUser() methodds ");
		return registeredUserListDetails;

	}

	@PutMapping("/updateLockedUnlockedFlag/{lockedUnlockedValue}/{registeredEmailAddress}")
	public boolean toLockedUnlockedTheUserAccount(@PathVariable("lockedUnlockedValue") String lockedUnlockedValue,
			@PathVariable("registeredEmailAddress") String registeredEmailAddress) throws SQLException {
		boolean accountLockedUnlocked = true;
		logger.info("In  toLockedUnlockedTheUserAccount() method" + lockedUnlockedValue + ", registeredEmailAddress :"
				+ registeredEmailAddress);
		// updating account_locked in user table
		loginDao.toUpdateLockedUnlockedCheckedToDb(registeredEmailAddress, lockedUnlockedValue);

		return accountLockedUnlocked;

	}

	// controller method to upload CSV file
	@PostMapping("/uploadCsvFile/{emailAddress}/{password}")
	public Map<String, Object> uploadCsvFile(@RequestParam("file") MultipartFile file,
			@PathVariable("emailAddress") String emailAddress, @PathVariable("password") String password)
			throws EOFException, ClassNotFoundException, IOException, SQLException {

		logger.info("uploadCsvFile() method start");

		List<BulkCsvModel> bulkCsvModelList = new ArrayList<>();
		Map<String, Object> mapObject = new HashMap<>();

		byte[] bytes = file.getBytes();
		ByteArrayInputStream inputFilestream = new ByteArrayInputStream(bytes);

		int totalInputCsvFileRowCount = 0;
		String uniqueJobId = UUID.randomUUID().toString();

		String generatedString = RandomStringUtils.randomAlphabetic(5);
		String fileNameWithGenratedString = "BulkCsv-" + generatedString + ".csv";
		boolean isUploadedCsvFileIsEmptyFlag = false;
		// checking email address and password already exist in database

		if (loginDao.checkEmailAddressPasswordExistInDb(emailAddress, password)) {

			logger.info("email address and password is exist in database");
			try {

				bulkCsvModelList = ReadCsvFile.readCsvFile(inputFilestream);

			} catch (Exception e) {
				e.printStackTrace();
				logger.error("Exception " + e);

			}
			if (!bulkCsvModelList.isEmpty() && bulkCsvModelList != null) {

				asynchronousClass.asynchronousCreditFraudAndSmartleadsApiCall(bulkCsvModelList,
						fileNameWithGenratedString, emailAddress);

				mapObject.put("genratedCsvFile", fileNameWithGenratedString);
				mapObject.put("isUploadedCsvFileIsEmpty", isUploadedCsvFileIsEmptyFlag);

				totalInputCsvFileRowCount = bulkCsvModelList.size();
				mapObject.put("totalInputCsvFileRowCount", totalInputCsvFileRowCount);
				mapObject.put("jobId", uniqueJobId);

				logger.info("uploadCsvFile() method end");

				return mapObject;
			}

			else {

				logger.info(" The uploaded csv file is empty , So not continue to read");
				mapObject.put("isUploadedCsvFileIsEmptyFlag", true);
				logger.info("uploadCsvFile() method end");
				return mapObject;

			}
		} else {

			logger.info("email address and password not exist in database");
			return mapObject;

		}

	}

	// for downloading generated CSV file

	// method to download CSV file
	@PostMapping("downloadcsvfile/{csvFileName:.+}")
	public void downloadGenratedCsvFile(HttpServletResponse response, @PathVariable("csvFileName") String fileName)
			throws IOException, ClassNotFoundException {

		Properties prop = util.readPropertiesFile("application.properties");
		String csvFilePath = prop.getProperty("writeCsvFilePath");

		logger.info("In downloadGenratedCsvFile() method  for file name" + fileName);

		String csvFileName = csvFilePath + fileName;
		try {
			File file = new File(csvFileName);

			if (file.exists() && !file.isDirectory()) {

				Files.copy(file.toPath(), response.getOutputStream());
				String mimeType = URLConnection.guessContentTypeFromName(file.getName());
				String contentDisposition = String.format("attachment; filename=%s", file.getName());
				int fileSize = Long.valueOf(file.length()).intValue();

				response.setContentType(mimeType);
				response.setHeader("Content-Disposition", contentDisposition);
				response.setContentLength(fileSize);
				logger.info("csv file is downloded successfully with csv file name :" + csvFileName);
			} else {
				logger.info(" csv file is not present in the file path");
			}

		} catch (FileNotFoundException fe) {
			fe.printStackTrace();
			logger.error(" FileNotFoundException Exception " + fe);
		} catch (IOException ie) {
			ie.printStackTrace();
			logger.error(" IOException Exception " + ie);

		} catch (Exception e) {
			e.printStackTrace();
			logger.error(" Exception " + e);

		}

	}

	// to get the row processing count
	@GetMapping("getRowProcessingCountCsvFileOfApi/{csvFileName:.+}")
	public int getRowProcessingCountCsvFileOfApi(HttpServletResponse response,
			@PathVariable("csvFileName") String fileName) {
		BufferedReader bufferedReader = null;
		Properties prop = null;
		String csvFilePath = "";
		int count = 0;
		File file = null;
		int iteration = 0;

		logger.info("in getRowProcessingCountCsvFileOfApi() method");
		try {

			Util util = new Util();
			prop = util.readPropertiesFile("application.properties");
			csvFilePath = prop.getProperty("writeCsvFilePath");
			file = new File(csvFilePath + fileName);

		} catch (Exception e) {
			e.printStackTrace();
			logger.error(" Exception " + e);

		}
		try {
			if (file.exists()) {
				bufferedReader = new BufferedReader(new FileReader(csvFilePath + fileName));
				String input;

				try {
					while ((input = bufferedReader.readLine()) != null) {
						if (iteration == 0) {
							iteration++;
							continue;
						}
						count++;
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					logger.error(" IOException " + e);

				}
			}
		}

		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(" FileNotFoundException " + e);
		}

		logger.info("row processing count is  : " + count);
		return count;

	}

	// method to clear user history
	@RequestMapping(value = "/clearUserHistory", method = RequestMethod.POST)
	public Map<String, Object> clearUserHistory(@RequestBody CreaditAndFraudModel creaditAndFraudModel) {

		logger.info(" In clearUserHistory() method");
		Map<String, Object> mapObject = new HashMap<>();

		try {
			userHistoryDao.clearUserHistoryData(creaditAndFraudModel.getUserHistoryData());

			// Generate randomFileName
			String generatedString = RandomStringUtils.randomAlphabetic(5);
			String fileNameWithGenratedString = "creaditscore-" + generatedString + ".xlsx";

			logger.info("Genrated file Name is: " + fileNameWithGenratedString);

			// after the deleting the history data , getting new history data

			List<UserHistoryModel> userHistoryModelList = userHistoryDao.getAllUserHistoryDeatilsList(
					creaditAndFraudModel.getFirstName(), creaditAndFraudModel.getLastName(),
					creaditAndFraudModel.getIdNumber(), creaditAndFraudModel.getStartDate(),
					creaditAndFraudModel.getEndDate(), fileNameWithGenratedString, "admin",
					creaditAndFraudModel.getUserEmailAddress());
			mapObject.put("genratedReportFileName", fileNameWithGenratedString);
			mapObject.put("listOfSearchUserHistory", userHistoryModelList);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(" SQLException " + e);
		}
		return mapObject;

	}

}
